import * as jwt from 'jsonwebtoken';
import { Component, Logger } from '@nestjs/common';
import { JWT_SECRET } from '../../../config';
import { CustomerService } from '../../shopify-app/customer/customer.service';
import { Customer } from '../../shopify-app/customer/Customer';

@Component()
export class XmppAuthService {
  constructor(
    private readonly customerService: CustomerService,
    private readonly logger: Logger,
  ) { }

  async validateCustomerCreds(username: string, token: string): Promise<Customer> {
    let payload: any = {};
    try {
      payload = await (new Promise((res, rej) => {
        jwt.verify(token, JWT_SECRET, (err, decoded) => {
          if (err) {
            rej(err);
          } else {
            res(decoded);
          }
        });
      }));
    } catch (err) {
      throw new InvalidCustomerCreds('Invalid token');
    }

    if (payload.username !== username)
      throw new InvalidCustomerCreds('Inconsistent credentials');

    let customer = await this.customerService.getCustomerByUsername(payload.username);

    if (!customer) {
      this.logger.log('creating unstored customer');
      customer = await this.customerService.createUnstoredCustomer(payload.username, payload.shop);
      this.logger.log(`created customer: ${customer}`);
    }

    return customer;
  }

  async issueCustomerToken(shopDomain: string, expiresIn: number = 7776000): Promise<any> {
    const customer = await this.customerService.createShopCustomer(shopDomain);

    const payload = {
      sub: customer.username,
      username: customer.username,
      shop: customer.shop.domain,
    };

    const token = new Promise((res, rej) => {
      jwt.sign(payload, JWT_SECRET, { expiresIn, issuer: 'ChatbotProj' }, (err, signedToken) => {
        if (err) {
          rej(err);
        } else {
          res(signedToken);
        }
      });
    });

    return token;
  }
}

export class InvalidCustomerCreds extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, InvalidCustomerCreds.prototype);
    this.name = 'InvalidCustomerCreds';
  }
}

export class InvalidUUID extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, InvalidUUID.prototype);
    this.name = 'InvalidUUID';
  }
}