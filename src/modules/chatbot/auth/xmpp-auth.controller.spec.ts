import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import express from 'express';
import * as nock from 'nock';
import { XmppAuthController } from './xmpp-auth.controller';
import { XmppAuthService } from './xmpp-auth.service';
import { JwtAuthService } from '../../shopify-app';
import { Customer } from './Customer';
import { Agent } from '../../shopify-app/agent/Agent';
import { AgentRoles } from '../../shopify-app/agent/AgentRolesEnum';
import * as jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../../../config';
import * as isUuid from 'is-uuid';
import { ShopifyStoreService, ShopifyMemoryStoreService } from '../../shopify';

const mockAgentUsername = 'johndoe';
const mockAgentPass = 'abc123';
const mockShopName = 'fakeshop';
const mockShop = mockShopName + '.myshopify.com';
const mockAgent: Agent = {
  id: 1,
  username: mockAgentUsername,
  role: AgentRoles.AGENT_ADMIN,
  info: {
    job: null,
    avatarSrc: null,
    name: 'John Doe',
  },
  shop: null,
};

const mockCustomer: Customer = {
  username: 'c51c80c2-66a1-442a-91e2-4f55b4256a72',
  idShopify: null,
  phone: null,
  name: null,
  email: null,
  lastVisitedUrl: null,
  avatarSrc: null,
  device: null,
  shop: null,
};

const mockCustomerTokenPayload = {
  username: mockCustomer.username,
  sub: mockCustomer.username,
  shop: mockShop,
};
const mockCustomerToken = jwt.sign(mockCustomerTokenPayload, JWT_SECRET);

const mockXmppAuthServiceClazz = jest.fn(() => {
  return {
    validateCustomerCreds: jest.fn((() => Promise.resolve(mockCustomer))),
    issueCustomerToken: jest.fn(() => Promise.resolve(mockCustomerToken)),
  };
});

const mockXmppAuthService = new mockXmppAuthServiceClazz();

const mockJwtAuthServiceClazz = jest.fn(() => {
  return {
    validateUserCreds: jest.fn((() => Promise.resolve(mockAgent))),
  };
});

const mockJwtAuthService = new mockJwtAuthServiceClazz();

describe('XmppAuthController', () => {
  let module: TestingModule;

  const server = express();

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect('127.0.0.1');

    module = await Test.createTestingModule({
      controllers: [
        XmppAuthController,
      ],
      components: [
        Logger,
        {
          provide: XmppAuthService,
          useValue: mockXmppAuthService,
        },
        {
          provide: JwtAuthService,
          useValue: mockJwtAuthService,
        },
        {
          provide: ShopifyStoreService,
          useValue: ShopifyMemoryStoreService,
        },
      ],
    }).compile();

    (server as any).enable('trust proxy');
    const app = module.createNestApplication(server);
    await app.init();
  });

  let controller: XmppAuthController;
  beforeEach(() => {
    controller = module.get(XmppAuthController);
    mockJwtAuthService.validateUserCreds.mockClear();
    mockXmppAuthService.issueCustomerToken.mockClear();
    mockXmppAuthService.validateCustomerCreds.mockClear();
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should exist', () => {
    expect(controller).toBeTruthy();
  });

  it('should issue a token for customer xmpp client', async () => {
    const issuedToken = await controller.getCustomerToken(mockShop);

    const payload = jwt.verify(issuedToken.token, JWT_SECRET);

    expect(issuedToken).toBeTruthy();
    expect(issuedToken).toHaveProperty('token');
    expect(payload).toBeTruthy();
    expect(payload.shop).toBe(mockShop);
    expect(isUuid.anyNonNil(payload.username)).toBe(true);
  });

  it('should return string response on auth by customer xmpp client', async () => {
    const basicToken = Buffer.from(`${mockCustomer.username}:${mockCustomerToken}`).toString('base64');
    const mockAuthHeader = `Basic ${basicToken}`;

    const res = await controller.auth(mockAuthHeader);

    expect(res).toBe('');
    expect(mockXmppAuthService.validateCustomerCreds).toBeCalledWith(mockCustomer.username, mockCustomerToken);
  });

  it('should return string response on auth by agent xmpp client', async () => {
    const basicToken = Buffer.from(`${mockAgentUsername}.${mockShopName}:${mockAgentPass}`).toString('base64');
    const mockAuthHeader = `Basic ${basicToken}`;

    const res = await controller.auth(mockAuthHeader);

    expect(res).toBe('');
    expect(mockJwtAuthService.validateUserCreds).toBeCalledWith(mockAgentUsername, mockAgentPass, mockShop);
  });

});
