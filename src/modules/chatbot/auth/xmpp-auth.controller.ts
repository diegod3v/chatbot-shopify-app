import { Controller, Post, Get, Headers, Logger, Body, UnauthorizedException } from '@nestjs/common';
import { XmppAuthService, InvalidCustomerCreds } from './xmpp-auth.service';
import * as AuthHeader from 'auth-header';
import * as isUuid from 'is-uuid';
import { JwtAuthService } from '../../shopify-app';
import { ShopifyStoreService } from '../../shopify';

@Controller('xmpp')
export class XmppAuthController {
  constructor(
    private readonly logger: Logger,
    private readonly customerAuthService: XmppAuthService,
    private readonly agentAuthService: JwtAuthService,
    private readonly shopifyStoreService: ShopifyStoreService,
  ) { }

  @Get('auth')
  async auth(@Headers('authorization') authorization: string) {
    this.logger.log('Xmpp Login');

    let auth;
    try {
      auth = AuthHeader.parse(authorization);
    } catch (err) {
      this.logger.warn('Invalid xmpp auth header');
      throw new UnauthorizedException();
    }

    if (auth && auth.scheme !== 'Basic')
      throw new UnauthorizedException();

    const [username, token] = new Buffer(auth.token, 'base64').toString().split(':', 2);

    if (isUuid.anyNonNil(username)) {
      try {
        const customer = await this.customerAuthService.validateCustomerCreds(username, token);
        this.logger.log('customer xmpp login');

        if (!customer) throw new UnauthorizedException();
      } catch (err) {
        this.logger.warn(`Invalid xmpp auth creds - ${err}`);
        throw new UnauthorizedException();
      }
    } else {
      try {
        const [agentUsername, shopName] = username.split('#', 2);
        const shop = `${shopName}.myshopify.com`;

        const agent = await this.agentAuthService.validateAgentXmppCreds(agentUsername, shop, token);
        this.logger.log('agent xmpp login');

        if (!agent) throw new UnauthorizedException();
      } catch (err) {
        this.logger.warn('Invalid xmpp auth creds');
        throw new UnauthorizedException();
      }
    }
  }

  @Post('auth')
  async getCustomerToken(@Body('shop') shopDomain: string) {
    this.logger.log('Customer client creation: ' + shopDomain);

    if (!shopDomain) {
      this.logger.warn('Shop doesnt provided');
      throw new UnauthorizedException('Shop not provided');
    }

    const shop = await this.shopifyStoreService.getShopByDomain(shopDomain);

    if (!shop) {
      this.logger.warn('Trying to create customer token of not registered shop');
      throw new UnauthorizedException('Shop doesnt exist');
    }

    const token = await this.customerAuthService.issueCustomerToken(shopDomain);

    return { token };
  }
}

export interface XmppAuthCredsDTO {
  username: string;
  password: string;
}