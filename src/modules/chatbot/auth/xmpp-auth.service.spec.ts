import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import express from 'express';
import * as nock from 'nock';
import * as bcrypt from 'bcrypt';
import { B_COST, JWT_SECRET } from '../../../config';
import * as jwt from 'jsonwebtoken';
import { XmppAuthService, InvalidCustomerCreds } from './xmpp-auth.service';
import * as isUuid from 'is-uuid';
import { Customer } from './Customer';
import { CustomerService } from './customer.service';

const mockCustomer: Customer = {
  username: 'c51c80c2-66a1-442a-91e2-4f55b4256a72',
  idShopify: null,
  phone: null,
  name: null,
  email: null,
  lastVisitedUrl: null,
  avatarSrc: null,
  device: null,
  shop: null,
};

const mockCustomerServiceClazz = jest.fn(() => {
  return {
    createShopCustomer: jest.fn((() => Promise.resolve(mockCustomer))),
    getCustomerByUsername: jest.fn(() => Promise.resolve(mockCustomer)),
    getCustomerByIdShopify: jest.fn(() => Promise.resolve(mockCustomer)),
  };
});

const mockCustomerService = new mockCustomerServiceClazz();

describe('XmppAuthService', () => {
  let module: TestingModule;

  const server = express();

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect('127.0.0.1');

    module = await Test.createTestingModule({
      components: [
        {
          provide: CustomerService,
          useValue: mockCustomerService,
        },
        XmppAuthService,
      ],
    }).compile();

    (server as any).enable('trust proxy');
    const app = module.createNestApplication(server);
    await app.init();
  });

  let service: XmppAuthService;
  beforeEach(() => {
    service = module.get(XmppAuthService);
    mockCustomerService.createShopCustomer.mockClear();
    mockCustomerService.getCustomerByIdShopify.mockClear();
    mockCustomerService.getCustomerByUsername.mockClear();
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should exist', () => {
    expect(service).toBeTruthy();
  });

  it('it should issue token for customer user', async () => {
    const mockShop = 'fake.myshopify.com';

    const issuedToken = await service.issueCustomerToken(mockShop);

    const payload = jwt.verify(issuedToken, JWT_SECRET);

    expect(payload).toBeTruthy();
    expect(payload.shop).toBe(mockShop);
    expect(isUuid.anyNonNil(payload.username)).toBe(true);
    expect(mockCustomerService.createShopCustomer).toBeCalledWith(mockShop);
  });

  it('it should validate customer creds', async () => {
    const mockShop = 'fake.myshopify.com';
    const mockCustomerUsername = 'c51c80c2-66a1-442a-91e2-4f55b4256a72';
    const mockCustomerPassword = await service.issueCustomerToken(mockShop);

    const customer = await service.validateCustomerCreds(mockCustomerUsername, mockCustomerPassword);

    expect(customer).toBeTruthy();
    expect(customer.username).toBe(mockCustomer.username);
    expect(customer.name).toBe(mockCustomer.name);
    expect(mockCustomerService.getCustomerByUsername).toBeCalledWith(mockCustomerUsername);
  });

  it('it should validate customer creds and return credserror', async () => {
    const mockShop = 'fake.myshopify.com';
    const mockCustomerUsername = 'c51c80c2-66a1-442a-91e2-4f55b4256a72';
    let mockCustomerPassword = await service.issueCustomerToken(mockShop);

    const fakeSignature = 'cThIIoDvwdueQB468K5xDc5633seEFoqwxjF_xdJyQQ';
    const splitToken = mockCustomerPassword.split('.');
    splitToken[2] = fakeSignature;

    mockCustomerPassword = splitToken.join('.');

    try {
      const customer = await service.validateCustomerCreds(mockCustomerUsername, mockCustomerPassword);
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidCustomerCreds);
    }
  });
});
