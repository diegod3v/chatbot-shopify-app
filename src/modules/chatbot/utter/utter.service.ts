import { Component } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FlowNode } from '../../../modules/shopify-app/flow-config/FlowNode.entity';
import { ExtraInfoDTO } from './extraInfo.dto';
import { MessageDTO, MessageButton } from './message.dto';
import { pickBy, mapValues } from 'lodash';
import { CustomerService } from '../../../modules/shopify-app/customer/customer.service';
import { MailerService } from '../mailer/mailer.service';
import * as handlebars from 'handlebars';

const DEFAULT_MESSAGE = 'Losiento no pude entenderte. ¿Puedes reformular la pregunta?';
const CALL_HUMAN_MESSAGE = 'Quiero contactar a un humano';

@Component()
export class UtterService {
  constructor(
    @InjectRepository(FlowNode) private readonly flowNodeRepository: Repository<FlowNode>,
    private readonly customerService: CustomerService,
    private readonly mailerService: MailerService,
  ) {
  }

  async utter(message: string, extraInfo: ExtraInfoDTO): Promise<MessageDTO> {
    const { nlp, sourcePayload, user, context } = extraInfo;
    let nextNodeResponse: FlowNode;

    if (nlp) {
      const { entities: nlpEntities } = nlp;
      const intent = nlpEntities.intent;
      const mainIntent = intent && intent[0] && intent[0];
      const mainIntentValue = mainIntent && mainIntent.confidence > 0.6 && mainIntent.value;

      if (context.questionNodeId) {
        const confidentEntities = pickBy(nlpEntities, (entity) => entity[0] && entity[0].confidence > 0.6);
        const entities = mapValues(confidentEntities, (entity) => entity[0] && entity[0].value);

        const questionNode = await this.flowNodeRepository.findOne({ where: { id: context.questionNodeId } });
        const { payload: qNodePayload } = questionNode.info;
        const qNodeParsePayload = JSON.parse(qNodePayload);

        const { valueType, value: valueName } = qNodeParsePayload;

        const extractedValue = entities[valueType];

        await this.customerService.setCustomerContextVar(user.customer.username, valueName, extractedValue);

        nextNodeResponse = await this.flowNodeRepository.findOne({ where: { id: questionNode.nextNodeId } });
      } else {
        nextNodeResponse = await this.flowNodeRepository.findOne({ where: { intent: mainIntentValue } });
      }
    } else {
      const nextNodeId = sourcePayload.nextNodeId;
      nextNodeResponse = await this.flowNodeRepository.findOne({ where: { id: nextNodeId } });
    }

    if (nextNodeResponse) {
      if (nextNodeResponse.info.type === 'email') {
        const nodeResponsePayload = nextNodeResponse.info.payload;
        const parsedNodeResponsePayload = JSON.parse(nodeResponsePayload);
        const { text: messageTemplate } = parsedNodeResponsePayload;
        const messageTempalteCompiled = handlebars.compile(messageTemplate);

        const customerInternalContext = await this.customerService.getCustomerContext(user.customer.username);

        const messageText = messageTempalteCompiled({ ...context, ...customerInternalContext });
        await this.mailerService.sendNotificationMail(user.shopDomain, messageText);
        nextNodeResponse = await this.flowNodeRepository.findOne({ where: { id: nextNodeResponse.nextNodeId } });
        if (!nextNodeResponse)
          return this.generateTextMessage('procesando... Listo!');
      }
      return this.generateTemplateMessageFromNode(nextNodeResponse);
    } else {
      const callAgentButton: MessageButton = {
        type: 'postback',
        payload: {
          text: CALL_HUMAN_MESSAGE,
          callHuman: true,
          shop: user.shopDomain,
          customer: user.customer,
        },
        title: CALL_HUMAN_MESSAGE,
      };
      return this.generateTemplateMessage({ type: 'text', payload: { text: DEFAULT_MESSAGE } }, [callAgentButton]);
    }
  }

  processResponseNode(nextNodeResponse, message: string, extraInfo: ExtraInfoDTO) {
    const { nlp, sourcePayload, user, context } = extraInfo;
  }

  private generateTemplateMessageFromNode(node): MessageDTO {
    const parsedPayload = JSON.parse(node.info.payload);
    const nodeInfo = { ...node.info, payload: parsedPayload, currentNodeId: node.id };
    const nodeOptions = node.options;

    if (!nodeOptions || nodeOptions.length === 0) {
      return this.generateTextMessage(nodeInfo);
    }

    const buttons = nodeOptions.map(option => ({
      type: 'postback',
      payload: {
        currentNodeId: node.id,
        nextNodeId: option.nextNodeId,
        text: option.text,
      },
      title: option.text,
    }));

    return this.generateTemplateMessage(nodeInfo, buttons);
  }

  private generateTemplateMessage(nodeInfo: any, buttons: MessageButton[]): MessageDTO {
    return {
      nodeType: nodeInfo.type,
      currentNodeId: nodeInfo.currentNodeId,
      attachment: {
        type: 'template',
        payload: {
          template_type: 'button',
          text: nodeInfo.payload.text,
          buttons,
        },
      },
    };
  }

  private generateTextMessage(nodeInfo: any) {
    return {
      currentNodeId: nodeInfo.currentNodeId,
      nodeType: nodeInfo.type,
      text: nodeInfo.payload.text,
    };
  }
}