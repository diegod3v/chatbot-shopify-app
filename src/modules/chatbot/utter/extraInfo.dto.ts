export interface ExtraInfoDTO {
  sourcePayload?: any;
  context?: any;
  nlp?: any;
  session?: any;
  user: {
    customer: any,
    shopDomain: string,
  };
}