import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import express from 'express';
import * as nock from 'nock';
import { UtterService } from './utter.service';
import { ShopifyMemoryStoreService } from '../../shopify';
import { ShopifyApiService } from '../../shopify/shopify-api/shopify-api.service';

describe('UtterService', () => {
  let module: TestingModule;

  const server = express();

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect('127.0.0.1');

    module = await Test.createTestingModule({
      components: [
        UtterService,
        Logger,
        ShopifyMemoryStoreService,
        ShopifyApiService,
      ],
    }).compile();

    (server as any).enable('trust proxy');
    const app = module.createNestApplication(server);
    await app.init();
  });

  let service: UtterService;
  beforeEach(() => {
    service = module.get(UtterService);
  });

  afterEach(() => {
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should exist', () => {
    expect(service).toBeTruthy();
  });

  it('should call parse npl engine endpoint', () => {
  });
});
