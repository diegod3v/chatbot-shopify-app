export interface MessageDTO {
  nodeType: string;
  text?: string;
  currentNodeId?: string;
  attachment?: {
    type: 'template',
    payload: {
      template_type: 'list' | 'button',
      text: string,
      buttons?: MessageButton[]
      ,
    },
  };
}

export interface MessageButton {
  type: string;
  url?: string;
  payload?: any;
  title: string;
}