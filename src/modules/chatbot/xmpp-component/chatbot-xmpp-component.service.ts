import { Component, Logger } from '@nestjs/common';
import { ChatbotXmppComponent } from './ChatbotXmppComponent';
import { ShopifyApiService } from '../../shopify';
import { Component as XmppComponent } from '@xmpp/component';
import * as xml from '@xmpp/xml';
import { XMPP_COMPONENT_SECRET, XMPP_COMPONENT_HOST, XMPP_COMPONENT_PORT, XMPP_COMPONENT_DOMAIN, XMPP_DOMAIN } from '../../../config';
import { NlpService } from '../nlp/nlp.service';
import { UtterService } from '../utter/utter.service';
import { CustomerService } from '../../shopify-app/customer/customer.service';
import { MessageDTO } from '../utter/message.dto';
import { ExtraInfoDTO } from '../utter/extraInfo.dto';
import { AgentService } from '../../../modules/shopify-app/agent/agent.service';
import { pick } from 'lodash';

@Component()
export class ChatbotXmppComponentService implements ChatbotXmppComponent {
  private component: XmppComponent;

  constructor(
    private readonly logger: Logger,
    private readonly shopifyApi: ShopifyApiService,
    private readonly nlpService: NlpService,
    private readonly utterService: UtterService,
    private readonly customerService: CustomerService,
    private readonly agentService: AgentService,
  ) { }

  async handleMessage(stanza) {
    const originalDestination = stanza.attrs.to;
    const originalSender = stanza.attrs.from;
    const stanzaUserJid = originalSender;
    const userId = stanzaUserJid.split('@')[0];
    const message = stanza.getChildText('body', null);

    try {
      const parsedMessage = JSON.parse(message);
      const { text: messageText, nextNodeId, callHuman, context } = parsedMessage;
      const customer = await this.customerService.getCustomerByUsername(userId);
      const shopDomain = customer.shop.domain;
      const utterExtraInfo: ExtraInfoDTO = {
        context,
        sourcePayload: parsedMessage,
        user: { customer: pick(customer, ['id', 'username']), shopDomain },
      };

      if (callHuman) {
        const agent = await this.agentService.getAgentByShop(shopDomain);
        const agentJidUser = `${agent.username}#${agent.shop.domain.replace('.myshopify.com', '')}`;
        this.sendTransferIq(stanzaUserJid, agentJidUser);
        return;
      } else if (!nextNodeId) {
        const NlpInfo = await this.nlpService.parse(parsedMessage.text);
        utterExtraInfo.nlp = NlpInfo;
      }

      const botResponse = await this.utterService.utter(messageText, utterExtraInfo);

      this.sendMessage(stanzaUserJid, botResponse);

    } catch (err) {
      this.logger.warn(err);
    }

  }

  handleError(err) {
    this.logger.warn(err);
  }

  public start(): void {
    this.component = new XmppComponent();
    const component = this.component;

    component.start({
      uri: `xmpp://${XMPP_COMPONENT_HOST}:${XMPP_COMPONENT_PORT}`,
      domain: XMPP_COMPONENT_DOMAIN,
    });

    component.on('error', (err) => { this.handleError(err); });

    component.on('status', (status, value) => {
      this.logger.log(`XMPP-COMP: ${status} ${(value ? value.toString() : '')}`);
    });

    component.on('online', jid => {
      this.logger.log(`XMPP-COMP: xmpp component online as ${jid.toString()}`);
    });

    component.on('stanza', stanza => {
      if (stanza.name === 'message') {
        this.handleMessage(stanza);
      }
    });

    component.handle('authenticate', authenticate => {
      this.logger.log('Auth component');
      return authenticate(XMPP_COMPONENT_SECRET);
    });

  }

  private sendMessage(to: string, message: MessageDTO) {
    const stanzaResponse = (
      xml('message', { to, type: 'chat' },
        xml('body', {}, JSON.stringify(message)),
      )
    );

    this.component.send(stanzaResponse);
  }

  private sendIq(to: string, type: string, query: any) {
    const iqResponse = (
      xml('iq', { to, type },
        xml('query', {}, query))
    );
    this.component.send(iqResponse);
  }

  private sendTransferIq(to: string, jidUserTransfer: string) {
    this.sendIq(to, 'set', xml('jidUserTransfer', {}, jidUserTransfer));
  }

  private interconnectUsers(customer: string, agent: string) {
    this.sendTransferIq(customer, agent);
    this.sendTransferIq(agent, customer);
  }
}