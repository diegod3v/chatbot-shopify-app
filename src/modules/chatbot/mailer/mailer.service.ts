import { Component } from '@nestjs/common';
import * as sgMail from '@sendgrid/mail';
import { SG_KEY } from '../../../config';
import { ChatConfigService } from '../../../modules/shopify-app/chat-config/chat-config.service';

sgMail.setApiKey(SG_KEY);
const DEFAULT_SENDER = 'notifications@chatbotproj.com';

@Component()
export class MailerService {
  constructor(
    private readonly configService: ChatConfigService,
  ) { }

  async sendNotificationMail(shopDomain: string, message: string, textMessage?: string) {
    this.configService.setShopDomain(shopDomain);
    const shopConfig = await this.configService.getConfig();

    const msg = {
      to: shopConfig.email,
      from: DEFAULT_SENDER,
      subject: 'Notificación Chatbotproj',
      text: textMessage || message,
      html: message,
    };
    this.configService.clearShopDomain();
    return sgMail.send(msg);
  }
}