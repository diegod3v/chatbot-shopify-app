export interface ParseResponse {
  msg_id: string;
  _text: string;
  entities: any;
}