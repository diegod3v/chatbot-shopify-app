import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import express from 'express';
import * as nock from 'nock';
import { NlpService } from './nlp.service';
import axios from 'axios';
import { ConversationEvent } from './NLP';

jest.mock('axios', () => {
  return {
    get: jest.fn(() => Promise.resolve({ data: {} })),
    post: jest.fn(() => Promise.resolve({ data: {} })),
    put: jest.fn(() => Promise.resolve({ data: {} })),
  };
});

describe('NlpService', () => {
  let module: TestingModule;

  const server = express();
  const mockNlpUri = 'http://localhost:5005';
  const mockUserId = 'default';

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect('127.0.0.1');

    module = await Test.createTestingModule({
      components: [
        {
          provide: NlpService,
          useValue: new NlpService(mockNlpUri),
        },
      ],
    }).compile();

    (server as any).enable('trust proxy');
    const app = module.createNestApplication(server);
    await app.init();
  });

  let service: NlpService;
  beforeEach(() => {
    service = module.get(NlpService);
    service.userId = mockUserId;
  });

  afterEach(() => {
    (axios.get as any).mockClear();
    (axios.post as any).mockClear();
    (axios.put as any).mockClear();
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should exist', () => {
    expect(service).toBeTruthy();
  });

  it('should call parse npl engine endpoint', () => {
    const parseEndpoint = `${mockNlpUri}/conversations/${mockUserId}/parse`;
    const message = 'Hi there!';

    service.parse(message);

    expect(axios.post).toBeCalledWith(parseEndpoint, { query: message });
  });

  it('should call continue nlp engine endpoint', () => {
    const continueEndpoint = `${mockNlpUri}/conversations/${mockUserId}/continue`;
    const action = 'utter_greet';

    service.progress(action);

    expect(axios.post).toBeCalledWith(continueEndpoint, { executed_action: action });
  });

  it('should call get tracker nlp engine endpoint', () => {
    const trackerEndpoint = `${mockNlpUri}/conversations/${mockUserId}/tracker`;

    service.getState();

    expect(axios.get).toBeCalledWith(trackerEndpoint);
  });

  it('should call setState nlp engine endpoint', () => {
    const trackerEventsEndpoint = `${mockNlpUri}/conversations/${mockUserId}/tracker/events`;
    const events: ConversationEvent[] = [{ event: 'slot', name: 'user_name', value: 'John Doe' }];

    service.setState(events);

    expect(axios.post).toBeCalledWith(trackerEventsEndpoint, events);
  });

});
