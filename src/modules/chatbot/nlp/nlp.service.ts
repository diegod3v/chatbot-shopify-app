import { ParseResponse } from './NLP';
import { Component } from '@nestjs/common';
import axios from 'axios';
import { WIT_KEY } from '../../../config';

const witApi = axios.create({
  baseURL: 'https://api.wit.ai/',
  headers: { Authorization: `Bearer ${WIT_KEY}` },
  params: { v: '20190414' },
});

@Component()
export class NlpService {
  parse(text: string): Promise<ParseResponse> {
    return witApi.get('/message', { params: { q: text } })
      .then(response => {
        return response.data as Promise<ParseResponse>;
      });
  }
}