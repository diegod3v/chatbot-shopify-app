import { Module, Logger } from '@nestjs/common';
import { ChatbotXmppComponentService } from './xmpp-component/chatbot-xmpp-component.service';
import { NlpService } from './nlp/nlp.service';
import { XmppAuthService } from './auth/xmpp-auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UtterService } from './utter/utter.service';
import { NodeOption } from '../../modules/shopify-app/flow-config/NodeOption.entity';
import { FlowNode } from '../../modules/shopify-app/flow-config/FlowNode.entity';
import { FlowConfig } from '../../modules/shopify-app/flow-config/FlowConfig.entity';
import { NodeInfo } from '../../modules/shopify-app/flow-config/NodeInfo.entity';
import { MailerService } from './mailer/mailer.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([NodeOption, FlowNode, FlowConfig, NodeInfo]),
  ],
  controllers: [],
  components:
    [
      Logger,
      ChatbotXmppComponentService,
      NlpService,
      XmppAuthService,
      UtterService,
      MailerService,
    ],
  exports: [ChatbotXmppComponentService, XmppAuthService],
})
export class ChatbotModule {
}
