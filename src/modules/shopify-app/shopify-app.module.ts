import { Logger, MiddlewaresConsumer, Module, NestModule } from '@nestjs/common';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { REQUIRED_PERMISSIONS, SHOPIFY_API_KEY, SHOPIFY_REDIRECT_URL, SHOPIFY_SECRET } from '../../config';
import { ShopData, ShopifyAuthController, ShopifyAuthMiddleware, ShopifyDBStoreService, ShopifyModule } from '../shopify';
import { AgentDBE } from './agent/Agent.entity';
import { AgentService } from './agent/agent.service';
import { AgentInfoDBE } from './agent/AgentInfo.entity';
import { ApiAgentPublicController } from './api-agent-public.controller';
import { ApiAgentController } from './api-agent.controller';
import { JwtAuthMiddleware } from './auth/jwt-auth.middleware';
import { JwtAuthService } from './auth/jwt-auth.service';
import { ShopInfoService } from './shop-info/shop-info.service';
import { ShopAddressDBE } from './shop-info/ShopAddress.entity';
import { ShopInfoDBE } from './shop-info/ShopInfo.entity';
import { ShopifyAuthCallbacksImpl } from './shopify-auth-callbacks-impl.service';
import { ChatbotModule } from 'modules/chatbot';
import { FlowConfigService } from './flow-config/flow-config.service';
import { FlowConfig } from './flow-config/FlowConfig.entity';
import { FlowNode } from './flow-config/FlowNode.entity';
import { NodeOption } from './flow-config/NodeOption.entity';
import { Position } from './flow-config/Position.entity';
import { NodeInfo } from './flow-config/NodeInfo.entity';
import { CustomerService } from './customer/customer.service';
import { CustomerDBE } from './customer/Customer.entity';
import { NlpConfigService } from './nlp-config/nlp-config.service';
import { ChatConfigService } from './chat-config/chat-config.service';
import { ChatConfig } from './chat-config/chat-config.entity';
import { CustomerChatController } from './customer-chat.controller';

@Module({
  imports: [
    ShopifyModule.forRoot({
      apiKey: SHOPIFY_API_KEY,
      secretKey: SHOPIFY_SECRET,
      redirectUrl: SHOPIFY_REDIRECT_URL,
      permissions: REQUIRED_PERMISSIONS,
    }, { storeService: ShopifyDBStoreService, authCallbacksImpl: ShopifyAuthCallbacksImpl }),
    TypeOrmModule.forFeature([
      ShopData, ShopInfoDBE,
      ShopAddressDBE, AgentDBE,
      AgentInfoDBE, CustomerDBE,
      FlowConfig, FlowNode, NodeOption, Position, NodeInfo,
      ChatConfig,
    ]),
  ],
  controllers: [
    ShopifyAuthController,
    ApiAgentController,
    ApiAgentPublicController,
    CustomerChatController,
  ],
  components: [
    Logger,
    AgentService,
    CustomerService,
    JwtAuthService,
    ShopInfoService,
    FlowConfigService,
    ChatConfigService,
    NlpConfigService,
  ],
  exports: [ShopifyModule, JwtAuthService],
})
export class ShopifyAppModule implements NestModule {
  constructor(
    private readonly agentService: AgentService,
    private readonly chatConfigService: ChatConfigService,
    private readonly flowConfigService: FlowConfigService,
    @InjectRepository(ShopData) private readonly shopRepository: Repository<ShopData>,
    private readonly logger: Logger,
  ) {
    if (process.env.NODE_ENV !== 'production') {
      this.createMockData();
    }
  }

  private async createMockData() {
    try {
      const testShop = new ShopData();
      testShop.domain = 'fake.myshopify.com';
      testShop.accessToken = 'd0sa9uf08ajdf0asd';
      await this.shopRepository.save(testShop);

      await this.agentService.createAdminAgentForNewShop(testShop.domain, 'admin');

      this.flowConfigService.setShopDomain(testShop.domain);
      await this.flowConfigService.createShopInitialConfig();

      this.chatConfigService.setShopDomain(testShop.domain);
      await this.chatConfigService.createInitialConfig();

      const seededAgents = await this.agentService.getShopAgents(testShop.domain);
      this.logger.log('Seedded Agents: ' + JSON.stringify(seededAgents));
    } catch (err) {
      this.logger.error(err);
    }
  }

  configure(consumer: MiddlewaresConsumer): void | MiddlewaresConsumer {
    consumer.apply(ShopifyAuthMiddleware).forRoutes(ShopifyAuthController);
    consumer.apply(JwtAuthMiddleware).forRoutes(ApiAgentController, CustomerChatController);
  }
}
