import { ShopData } from '../../shopify';

export interface Customer {
  idShopify: number;
  username: string;
  name: string;
  phone: string;
  email: string;
  lastVisitedUrl: string;
  device: string;
  avatarSrc: string;
  shop: ShopData;
}