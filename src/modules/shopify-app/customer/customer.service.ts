import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomerDBE } from './Customer.entity';
import * as uuid from 'uuid/v4';
import { Customer } from './Customer';
import { Logger, Component } from '@nestjs/common';
import { ShopifyStoreService } from '../../shopify';

const specialContextKeys = [
  'name',
  'email',
  'phone',
];

@Component()
export class CustomerService {
  constructor(
    @InjectRepository(CustomerDBE)
    private readonly customerRepository: Repository<CustomerDBE>,
    private readonly logger: Logger,
    private readonly shopStoreService: ShopifyStoreService,
  ) {
  }

  async createShopCustomer(shopDomain: string): Promise<Customer> {
    const shop = await this.shopStoreService.getShopByDomain(shopDomain);

    const nextCustomer = new CustomerDBE();
    nextCustomer.username = uuid();
    nextCustomer.shop = shop;

    await this.customerRepository.save(nextCustomer);

    return nextCustomer;
  }

  async createUnstoredCustomer(username: string, shopDomain: string): Promise<Customer> {
    const shop = await this.shopStoreService.getShopByDomain(shopDomain);

    const nextCustomer = new CustomerDBE();
    nextCustomer.username = username;
    nextCustomer.shop = shop;

    await this.customerRepository.save(nextCustomer);

    return nextCustomer;
  }

  getCustomerByUsername(username: string): Promise<Customer> {
    return this.customerRepository.findOne({ username }, { relations: ['shop'] });
  }

  getCustomerByIdShopify(idShopify: number): Promise<Customer> {
    return this.customerRepository.findOne({ idShopify }, { relations: ['shop'] });
  }

  async setCustomerContextVar(username: string, key: string, value: any) {
    const customer = await this.customerRepository.findOne({ username }, { relations: ['shop'] });
    const { context: customerContext } = customer;

    const parsedContext = JSON.parse(customerContext);
    const nextContext = { ...parsedContext, [key]: value };

    const nextCustomer = { context: nextContext };

    if (specialContextKeys.indexOf(key) > -1) {
      nextCustomer[key] = value;
    }

    await this.customerRepository.update({ username }, nextCustomer);
    return this.customerRepository.findOne({ username }, { relations: ['shop'] });
  }

  async getCustomerContext(username: string) {
    const customer = await this.customerRepository.findOne({ username }, { relations: ['shop'] });
    const { context: customerContext } = customer;

    const parsedContext = JSON.parse(customerContext);
    return parsedContext;
  }
}