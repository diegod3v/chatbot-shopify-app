import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { Customer } from './Customer';
import { ShopData } from '../../shopify';

@Entity('Customer')
export class CustomerDBE implements Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  idShopify: number;

  @Column()
  username: string;

  @Column({ default: 'Anonimo' })
  name: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  lastVisitedUrl: string;

  @Column({ nullable: true })
  device: string;

  @Column({ nullable: true })
  context: string;

  @Column({ nullable: true })
  avatarSrc: string;

  @ManyToOne(type => ShopData, { onDelete: 'CASCADE' })
  shop: ShopData;
}