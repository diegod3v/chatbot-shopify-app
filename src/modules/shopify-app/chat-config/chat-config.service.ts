import { Component } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ChatConfig } from './chat-config.entity';
import { ChatConfigDTO } from './chat-config.dto';
import { omit } from 'lodash';

@Component()
export class ChatConfigService {
  private shopDomain: string;

  constructor(
    @InjectRepository(ChatConfig) private readonly chatConfigRepository: Repository<ChatConfig>,
  ) {
  }

  setShopDomain(shopDomain: string) {
    this.shopDomain = shopDomain;
  }

  clearShopDomain() {
    this.shopDomain = undefined;
  }

  deteleWholeConfig() {
    return this.chatConfigRepository.delete({ shopDomain: this.shopDomain });
  }

  createInitialConfig(): Promise<ChatConfig> {
    const defaultConfig = this.chatConfigRepository.create({
      agentName: 'Pedro Paramo',
      botName: 'Hugo Bot',
      shopDomain: this.shopDomain,
    });

    return this.chatConfigRepository.save(defaultConfig);
  }

  updateConfig(nextChatConfig: ChatConfigDTO): Promise<ChatConfig> {
    this.chatConfigRepository.update({ shopDomain: this.shopDomain }, omit(nextChatConfig, ['id', 'shopDomain']));
    return this.chatConfigRepository.findOne({ where: { shopDomain: this.shopDomain } });
  }

  getConfig() {
    return this.chatConfigRepository.findOne({ where: { shopDomain: this.shopDomain } });
  }

}