import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ShopData } from '../../shopify';

@Entity('ChatConfig')
export class ChatConfig {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'Bot' })
  botName: string;

  @Column({ default: 'Juan' })
  agentName: string;

  @Column({ nullable: true })
  email: string;

  @Column({ unique: true })
  shopDomain: string;
}