export interface ChatConfigDTO {
  id?: number;
  botName: string;
  agentName: string;
  shopDomain?: string;
}