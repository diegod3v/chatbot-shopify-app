export interface ShopAddress {
  address1: string;
  address2: string;
  city: string;
  province: string;
  country: string;
  zip: string;
}