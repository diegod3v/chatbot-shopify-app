import { ShopInfo } from './ShopInfo';
import { Entity, PrimaryGeneratedColumn, Column, Connection, OneToOne, JoinColumn } from 'typeorm';
import { ShopData } from '../../shopify';
import { ShopAddressDBE } from './ShopAddress.entity';

@Entity('ShopInfo')
export class ShopInfoDBE implements ShopInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  contactEmail: string;

  @Column({ nullable: true })
  currency: string;

  @Column({ nullable: true })
  externalDomain: string;

  @Column({ nullable: true })
  moneyFormat: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true, type: 'bigint' })
  shopifyId: number;

  @Column({ nullable: true })
  email: string;

  @OneToOne(type => ShopData, { onDelete: 'CASCADE' })
  @JoinColumn()
  shop: ShopData;

  @OneToOne(type => ShopAddressDBE, { eager: true, cascade: true })
  @JoinColumn()
  address: ShopAddressDBE;
}