import { ShopAddress } from './ShopAddress';
import { PrimaryGeneratedColumn, Entity, Column } from 'typeorm';

@Entity('ShopAddress')
export class ShopAddressDBE implements ShopAddress {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: true})
  address1: string;

  @Column({nullable: true})
  address2: string;

  @Column({nullable: true})
  city: string;

  @Column({nullable: true})
  province: string;

  @Column({nullable: true})
  country: string;

  @Column({nullable: true})
  zip: string;
}