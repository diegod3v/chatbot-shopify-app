import { Component } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ShopInfo } from './ShopInfo';
import { ShopifyStoreService } from '../../shopify';
import { InjectRepository } from '@nestjs/typeorm';
import { ShopInfoDBE } from './ShopInfo.entity';
import { ShopAddressDBE } from './ShopAddress.entity';
import { IShop } from 'shopify-api-node';

@Component()
export class ShopInfoService {
  constructor(
    private readonly shopifyStoreService: ShopifyStoreService,
    @InjectRepository(ShopInfoDBE) private readonly shopInfoRepository: Repository<ShopInfoDBE>,
    @InjectRepository(ShopAddressDBE) private readonly shopAddressRepository: Repository<ShopInfoDBE>,
  ) { }

  async saveShopInfoFromShopifyInfo(domain: string, info: IShop): Promise<ShopInfo> {
    const shop = await this.shopifyStoreService.getShopByDomain(domain);

    const nextShopInfo = new ShopInfoDBE();
    const shopAddress = new ShopAddressDBE();

    shopAddress.address1 = info.address1;
    shopAddress.address2 = info.address2;
    shopAddress.city = info.city;
    shopAddress.country = info.country;
    shopAddress.province = info.province;
    shopAddress.zip = info.zip;

    nextShopInfo.shop = shop;
    nextShopInfo.address = shopAddress;

    nextShopInfo.contactEmail = info.email;
    nextShopInfo.externalDomain = info.domain;
    nextShopInfo.moneyFormat = info.money_format;
    nextShopInfo.phone = info.phone;
    nextShopInfo.shopifyId = info.id;
    nextShopInfo.email = info.email;
    nextShopInfo.moneyFormat = info.money_format;
    nextShopInfo.currency = info.currency;

    return this.shopInfoRepository.save(nextShopInfo);
  }

  getShopInfo(domain: string): Promise<ShopInfo> {
    return this.shopInfoRepository.createQueryBuilder('shopInfo')
      .innerJoin('shopInfo.shop', 'shop')
      .innerJoinAndSelect('shopInfo.address', 'address')
      .where('shop.domain = :domain', { domain })
      .getOne();
  }

  deleteShopInfo(domain: string) {
    return this.shopInfoRepository.createQueryBuilder('shopInfo')
      .innerJoin('shopInfo.shop', 'shop')
      .innerJoinAndSelect('shopInfo.address', 'address')
      .where('shop.domain = :domain', { domain })
      .delete();
  }
}