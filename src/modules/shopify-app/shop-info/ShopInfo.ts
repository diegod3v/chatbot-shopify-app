import { ShopAddress } from './ShopAddress';
import { ShopData } from '../../shopify';

export interface ShopInfo {
  contactEmail: string;
  currency: string;
  externalDomain: string;
  moneyFormat: string;
  phone: string;
  shopifyId: number;
  email: string;
  shop: ShopData;
  address: ShopAddress;
}