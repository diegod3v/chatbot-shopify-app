import { Body, Controller, Logger, Post, UseGuards, Get } from '@nestjs/common';
import { JwtAuthService } from './auth/jwt-auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { JwtAuthProtected } from './auth/jwt-auth.decorator';
import { ChatConfigService } from './chat-config/chat-config.service';
import { Customer } from './customer/Customer';
import { AppUser } from './auth/app-user.decorator';
import { pick } from 'lodash';
import { Agent } from './agent/Agent';
import { ChatConfigDTO } from './chat-config/chat-config.dto';

@Controller('api/chat')
@UseGuards(JwtAuthGuard)
export class CustomerChatController {
  constructor(
    private readonly configService: ChatConfigService,
  ) { }

  @Get('config')
  @JwtAuthProtected()
  async getConfig(@AppUser() user: Agent | Customer) {
    this.configService.setShopDomain(user.shop.domain);
    const config = await this.configService.getConfig();
    const chatConfig = pick(config, ['botName', 'agentName', 'email']);
    this.configService.clearShopDomain();
    return chatConfig;
  }

  @Post('config')
  @JwtAuthProtected()
  async setConfig(@AppUser() user: Agent | Customer, @Body() nextConfig: ChatConfigDTO) {
    this.configService.setShopDomain(user.shop.domain);
    const config = await this.configService.updateConfig(nextConfig);
    const chatConfig = pick(config, ['id', 'botName', 'agentName', 'email']);
    this.configService.clearShopDomain();
    return chatConfig;
  }
}

