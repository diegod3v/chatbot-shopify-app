import { Component } from '@nestjs/common';
import axios from 'axios';
import { WIT_KEY } from '../../../config';

const witApi = axios.create({
  baseURL: 'https://api.wit.ai/',
  headers: { Authorization: `Bearer ${WIT_KEY}` },
  params: { v: '20190414' },
});

@Component()
export class NlpConfigService {
  getIntentList(): Promise<string[]> {
    return witApi.get('/entities/intent')
      .then(response => {
        const { data: { values } } = response;
        const intents = values.map((val) => val.value);
        return intents as Promise<string[]>;
      });
  }
}