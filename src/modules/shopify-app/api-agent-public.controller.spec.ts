import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as bcrypt from 'bcrypt';
import { B_COST } from '../../config';
import { Agent } from './agent/Agent';
import { AgentService } from './agent/agent.service';
import { AgentRoles } from './agent/AgentRolesEnum';
import { ApiAgentPublicController, LoginDataDTO } from './api-agent-public.controller';
import { JwtAuthService } from './auth/jwt-auth.service';

const mockUsername = 'johndoe';
const mockPass = 'abc123';
const mockShop = 'fakeshop.myfakefy.com';
const mockAgent: Agent = {
  id: 1,
  username: mockUsername,
  role: AgentRoles.AGENT_ADMIN,
  info: {
    job: null,
    avatarSrc: null,
    name: 'John Doe',
  },
  shop: null,
};

const mockAgentWithPass = { ...mockAgent, password: bcrypt.hashSync(mockPass, B_COST) };

const mockAgentServiceClazz = jest.fn(() => {
  return {
    getShopAgentByUsername: jest.fn(() => mockAgent),
    getShopAgentByUsernameWithPass: jest.fn(() => mockAgentWithPass),
  };
});

const mockAgentService = new mockAgentServiceClazz();

describe('ApiAgentController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      components: [
        Logger,
        JwtAuthService,
        {
          provide: AgentService,
          useValue: mockAgentService,
        },
      ],
      controllers: [ApiAgentPublicController],
    }).compile();
  });

  describe('agent login', () => {
    it('should return jwt on agent login with short expiry date', async () => {
      const apiAgentController = app.get<ApiAgentPublicController>(ApiAgentPublicController);
      const mockUserData: LoginDataDTO = {
        shop: mockShop,
        username: mockUsername,
        password: mockPass,
      };

      const jwt = await apiAgentController.login(mockUserData);

      expect(jwt).toHaveProperty('expires_in', 60 * 60 * 23);
      expect(jwt).toHaveProperty('token');
    });
  });
});
