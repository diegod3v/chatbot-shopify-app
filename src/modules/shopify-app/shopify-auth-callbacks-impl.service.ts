import { Component } from '@nestjs/common';
import { ShopifyAuthCallbacks, IShopifyUser, ShopifyApiService } from '../shopify';
import { AgentService } from './agent/agent.service';
import { SHOPIFY_STOREFRONT_CHAT_SCRIPT_URL } from '../../config';
import { ShopInfoService } from './shop-info/shop-info.service';
import { FlowConfigService } from './flow-config/flow-config.service';
import { ChatConfigService } from './chat-config/chat-config.service';

@Component()
export class ShopifyAuthCallbacksImpl extends ShopifyAuthCallbacks {

    constructor(
        private readonly shopifyApi: ShopifyApiService,
        private readonly agentService: AgentService,
        private readonly shopInfoService: ShopInfoService,
        private readonly flowConfigService: FlowConfigService,
        private readonly chatConfigService: ChatConfigService,
    ) {
        super();
    }

    beforeAuth(shop: string) {
        return Promise.resolve(true);
    }
    afterAuth(shopifyUser: IShopifyUser) {
    }
    beforeInstall(shop: string) {
        return Promise.resolve(true);
    }
    async afterInstall(shopifyUser: IShopifyUser) {
        // create scripttag to fetch chat UI in shop
        this.shopifyApi.accessToken = shopifyUser.accessToken;
        this.shopifyApi.shop = shopifyUser.domain;
        this.flowConfigService.setShopDomain(shopifyUser.domain);
        this.chatConfigService.setShopDomain(shopifyUser.domain);

        await this.shopifyApi.createScriptTag(SHOPIFY_STOREFRONT_CHAT_SCRIPT_URL);

        await this.agentService.createAdminAgentForNewShop(shopifyUser.domain, 'admin');
        await this.flowConfigService.createShopInitialConfig();
        await this.chatConfigService.createInitialConfig();

        const shopifyShopInfo = await this.shopifyApi.getShopInfo();

        await this.shopInfoService.saveShopInfoFromShopifyInfo(shopifyUser.domain, shopifyShopInfo);

        this.shopifyApi.accessToken = undefined;
        this.shopifyApi.shop = undefined;
        this.flowConfigService.clearShopDomain();
        this.chatConfigService.clearShopDomain();
    }
    async beforeUninstall(shop: string) {
        this.flowConfigService.setShopDomain(shop);
        this.chatConfigService.setShopDomain(shop);
        await this.flowConfigService.deleteWholeConfig();
        await this.chatConfigService.deteleWholeConfig();
        await this.shopInfoService.deleteShopInfo(shop);
        this.flowConfigService.clearShopDomain();
        this.chatConfigService.clearShopDomain();
    }
    afterUninstall(shop: string) {
    }
}
