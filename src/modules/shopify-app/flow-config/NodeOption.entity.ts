import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne } from 'typeorm';
import { FlowNode } from './FlowNode.entity';

@Entity('NodeOption')
export class NodeOption {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => FlowNode, { onDelete: 'CASCADE' })
  flowNode: FlowNode;

  @Column()
  text: string;

  @Column({ default: '' })
  nextNodeId: string;
}