import { FlowNode } from './FlowNode.entity';
import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from 'typeorm';

@Entity('FlowConfig')
export class FlowConfig {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(type => FlowNode, node => node.flowConfig, { eager: true, cascade: true, onDelete: 'CASCADE' })
  nodes: FlowNode[];

  @Column({ unique: true })
  shopDomain: string;
}