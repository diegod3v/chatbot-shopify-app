import { Component } from '@nestjs/common';
import { Repository } from 'typeorm';
import { FlowConfig } from './FlowConfig.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FlowNodeDTO } from './FlowNode.dto';
import { FlowNode } from './FlowNode.entity';
import { NodeOption } from './NodeOption.entity';
import { NodeOptionDTO } from './NodeOption.dto';
import { pick, omit, isEmpty } from 'lodash';
import { Position } from './Position.entity';
import { NodeInfo } from './NodeInfo.entity';

@Component()
export class FlowConfigService {
  private shopDomain: string;

  constructor(
    @InjectRepository(FlowConfig) private readonly flowConfigRepository: Repository<FlowConfig>,
    @InjectRepository(FlowNode) private readonly flowNodeRepository: Repository<FlowNode>,
    @InjectRepository(NodeOption) private readonly nodeOptionRepository: Repository<NodeOption>,
    @InjectRepository(Position) private readonly positionRepository: Repository<Position>,
    @InjectRepository(NodeInfo) private readonly nodeInfoRepository: Repository<NodeInfo>,
  ) {
  }

  setShopDomain(shopDomain: string) {
    this.shopDomain = shopDomain;
  }

  clearShopDomain() {
    this.shopDomain = undefined;
  }

  getConfigForShop(): Promise<FlowConfig> {
    return this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
  }

  async deleteWholeConfig() {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    this.flowConfigRepository.delete(flowConfig.id);
  }

  createShopInitialConfig(): Promise<FlowConfig> {
    const testFlowConfig = this.flowConfigRepository.create({
      nodes: [
        {
          position: { top: 10, left: 10 },
          info: {
            type: 'text',
            payload: '{"text": "Hola", "event": "site:enter"}',
          },
          options: [{ text: 'Opcion 1' }, { text: 'Opcion 2' }],
        },
        {
          position: { top: 30, left: 30 },
          info: {
            type: 'text',
            payload: '{"text": "Hola"}',
          },
          options: [{ text: 'Opcion 1' }],
        },
      ],
      shopDomain: this.shopDomain,
    });

    return this.flowConfigRepository.save(testFlowConfig)
  }

  async getNodesForShop(): Promise<FlowNode[]> {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    return this.flowNodeRepository.find({ flowConfig });
  }

  async getNodeById(id: string): Promise<FlowNode[]> {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    return this.flowNodeRepository.find({ where: { flowConfig, id } });
  }

  async addNode(nextNode: FlowNodeDTO): Promise<FlowNode> {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    const flowNode = this.flowNodeRepository.create(nextNode);
    flowNode.flowConfig = flowConfig;
    const savedNode = await this.flowNodeRepository.save(flowNode);
    savedNode.flowConfig = undefined;
    return savedNode;
  }

  async updateNode(id: string, nextNode: FlowNodeDTO): Promise<FlowNode> {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    const flowNode = await this.flowNodeRepository.findOne({ where: { flowConfig, id } });

    if (flowNode) {
      const nextNodeWithoutRelations = omit(nextNode, ['id', 'info', 'position', 'options'])

      if (nextNode.info)
        await this.nodeInfoRepository.update(flowNode.info.id, nextNode.info);

      if (nextNode.position)
        await this.positionRepository.update(flowNode.position.id, nextNode.position);

      if (!isEmpty(nextNodeWithoutRelations)) {
        await this.flowNodeRepository.update(id, nextNodeWithoutRelations);
      }

      return this.flowNodeRepository.findOne(id);
    }
  }

  async deleteNode(id: string) {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    const flowNode = await this.flowNodeRepository.findOne({ where: { flowConfig, id } });

    if (flowNode) {
      await this.flowNodeRepository.delete(id);
      return this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    }
  }

  async createNodeOption(nextNodeOption: NodeOptionDTO) {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    const flowNode = await this.flowNodeRepository.findOne({ where: { flowConfig, id: nextNodeOption.flowNode } });

    if (flowNode) {
      const nextNodeOptionFormatted = this.nodeOptionRepository.create({ ...nextNodeOption, flowNode });
      return this.nodeOptionRepository.save(nextNodeOptionFormatted);
    }
  }

  async updateNodeOption(id: string, nextNodeOption: NodeOptionDTO) {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    const flowNode = await this.flowNodeRepository.findOne({ where: { flowConfig, options: { id } } });

    if (flowNode) {
      await this.nodeOptionRepository.update(id, (nextNodeOption as NodeOption));
      return this.nodeOptionRepository.findOne(id);
    }
  }

  async deleteNodeOption(id: string, ) {
    const flowConfig = await this.flowConfigRepository.findOne({ shopDomain: this.shopDomain });
    const flowNode = await this.flowNodeRepository.findOne({ where: { flowConfig, options: { id } } });

    if (flowNode) {
      await this.nodeOptionRepository.delete(id);
      return this.flowNodeRepository.findOne({ where: { flowConfig } });
    }
  }
}