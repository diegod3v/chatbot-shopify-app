import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('NodeInfo')
export class NodeInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;

  @Column()
  payload: string;
}