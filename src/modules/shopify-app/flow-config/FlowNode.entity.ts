import { NodeInfo } from './NodeInfo.entity';
import { NodeOption } from './NodeOption.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, OneToOne, JoinColumn, OneToMany, Column } from 'typeorm';
import { FlowConfig } from './FlowConfig.entity';
import { Position } from './Position.entity';

@Entity('FlowNode')
export class FlowNode {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  intent: string | null;

  @Column({ nullable: true })
  event: string | null;

  @Column({ nullable: true })
  nextNodeId: string | null;

  @OneToOne(type => Position, { eager: true, cascade: true, onDelete: 'CASCADE' })
  @JoinColumn()
  position: Position;

  @OneToOne(type => NodeInfo, { eager: true, cascade: true, onDelete: 'CASCADE' })
  @JoinColumn()
  info: NodeInfo;

  @OneToMany(type => NodeOption, option => option.flowNode, { eager: true, cascade: true, onDelete: 'CASCADE' })
  options: NodeOption[];

  @ManyToOne(type => FlowConfig, { onDelete: 'CASCADE' })
  flowConfig: FlowConfig;
}