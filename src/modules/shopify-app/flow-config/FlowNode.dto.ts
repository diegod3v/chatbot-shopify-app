export interface FlowNodeDTO {
  id?: string;
  intent?: string;
  nextNodeId?: string;
  event?: string;
  readonly position?: {
    readonly top: number;
    readonly left: number;
  };
  readonly info?: {
    readonly type: string;
    readonly payload: string;
  };
  readonly options?: {
    readonly text: string;
    readonly nextNodeId: string;
  }[];
}