import { FlowNode } from './FlowNode.entity';

export interface NodeOptionDTO {
  text?: string;
  nextNodeId?: string;
  flowNode?: string | FlowNode;
}