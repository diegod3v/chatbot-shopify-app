import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('Position')
export class Position {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  top: number;

  @Column({ default: 0 })
  left: number;
}