import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { expect } from 'chai';
import { Repository } from 'typeorm';
import { ShopData, ShopifyDBStoreService, ShopifyStoreService } from '../../shopify';
import { AgentDBE } from './Agent.entity';
import { AgentInfoDBE } from './AgentInfo.entity';
import { AgentRoles } from './AgentRolesEnum';
import { AgentService } from './agent.service';

describe('Agent Service', () => {

  const mockShopDomain = 'test.myshopify.com';

  let module: TestingModule;
  let service: AgentService;

  beforeAll(async () => {
    const compiledModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqljs',
          autoSave: false,
          synchronize: true,
          entities: [
            __dirname + '/./*.entity{.ts,.js}',
            __dirname + '/../../shopify/**/*.entity{.ts,.js}',
          ],
        }),
        TypeOrmModule.forFeature([ShopData, AgentDBE, AgentInfoDBE]),
      ],
      components: [
        {
          provide: ShopifyStoreService,
          useClass: ShopifyDBStoreService,
        },
        {
          provide: 'ShopRepo',
          useFactory: (agentRepository: Repository<AgentDBE>) => {
            return agentRepository;
          },
          inject: [`${ShopData.name}Repository`],
        },
        AgentService,
      ],
    }).compile();

    module = compiledModule;
  });

  beforeAll(() => {
    const otherModule = module.select(TypeOrmModule);
    const shopifyShopRepository: Repository<ShopData> = module.get('ShopRepo');

    const shop = new ShopData();
    shop.accessToken = '8ud98saudas9d89sa9d8';
    shop.domain = mockShopDomain;
    shopifyShopRepository.save(shop);
  });

  beforeEach(() => {
    service = module.get(AgentService);
  });

  it('should exist', () => {
    expect(service).to.exist;
  });

  it('should create agent admin and normal agent for a new shop', async () => {
    await service.createAdminAgentForNewShop(mockShopDomain, 'mypass');

    const agents = await service.getShopAgents(mockShopDomain);
    const adminAgent = agents[0];

    expect(agents).length(1);
    expect(adminAgent.role).to.equal(AgentRoles.AGENT_ADMIN);
  });

  it('should fetch admin agent by username', async () => {
    const adminAgent = await service.getShopAgentByUsername(mockShopDomain, 'admin');

    expect(adminAgent).to.exist;
    expect(adminAgent.role).to.equal(AgentRoles.AGENT_ADMIN);
  });

  it('should fetch admin agent by id', async () => {
    const adminAgent = await service.getShopAgentById(mockShopDomain, 1);

    expect(adminAgent).to.exist;
    expect(adminAgent.role).to.equal(AgentRoles.AGENT_ADMIN);
  });
});