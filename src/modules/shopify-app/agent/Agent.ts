import { AgentInfo } from './AgentInfo';
import { ShopData } from '../../shopify';

export interface Agent {
  id: number;
  username: string;
  password?: string;
  role: string;
  info: AgentInfo;
  shop: ShopData;
}