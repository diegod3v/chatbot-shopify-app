export interface AgentInfo {
  name: string;
  job: string;
  avatarSrc: string;
}