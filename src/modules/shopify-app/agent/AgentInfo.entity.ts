import { AgentInfo } from './AgentInfo';
import { PrimaryGeneratedColumn, Entity, Column, OneToOne } from 'typeorm';
import { AgentDBE } from './Agent.entity';

@Entity('AgentInfo')
export class AgentInfoDBE implements AgentInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: true})
  name: string;

  @Column({nullable: true})
  job: string;

  @Column({ nullable: true })
  avatarSrc: string;

  @OneToOne(type => AgentDBE, { onDelete: 'CASCADE' })
  agent: AgentDBE;
}