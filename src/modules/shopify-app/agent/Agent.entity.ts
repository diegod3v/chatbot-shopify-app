import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ShopData } from '../../shopify';
import { Agent } from './Agent';
import { AgentInfoDBE } from './AgentInfo.entity';

@Entity('Agent')
export class AgentDBE implements Agent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column({ select: false })
  password: string;

  @OneToOne(type => AgentInfoDBE, { cascade: true })
  @JoinColumn()
  info: AgentInfoDBE;

  @Column()
  role: string;

  @ManyToOne(type => ShopData, { onDelete: 'CASCADE', eager: true })
  shop: ShopData;
}