import { Component } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { Repository } from 'typeorm';
import { B_COST } from '../../../config';
import { ShopifyStoreService } from '../../shopify';
import { Agent } from './Agent';
import { AgentDBE } from './Agent.entity';
import { AgentInfoDBE } from './AgentInfo.entity';
import { AgentRoles } from './AgentRolesEnum';

const PASS_LENGHT: number = 8;

@Component()
export class AgentService {
  constructor(
    private readonly shopifyStoreService: ShopifyStoreService,
    @InjectRepository(AgentDBE) private readonly agentRepository: Repository<AgentDBE>,
  ) {
  }

  async createAdminAgentForNewShop(shopDomain: string, pass: string): Promise<any> {
    const storedAdminAgent = await this.agentRepository.createQueryBuilder('agent')
      .innerJoin('agent.shop', 'shop')
      .where('shop.domain = :shopDomain', { shopDomain })
      .andWhere('agent.role = :role', { role: AgentRoles.AGENT_ADMIN })
      .getOne();

    if (storedAdminAgent) {
      throw new Error('Agent Admin already exist');
    }

    const shop = await this.shopifyStoreService.getShopByDomain(shopDomain);

    const nextAdminAgent = new AgentDBE();
    nextAdminAgent.shop = shop;
    nextAdminAgent.username = 'admin';
    nextAdminAgent.password = await bcrypt.hash(pass, B_COST);
    const agentInfo = new AgentInfoDBE();
    agentInfo.job = 'Administrator';
    agentInfo.name = 'Shop';
    nextAdminAgent.info = agentInfo;
    nextAdminAgent.role = AgentRoles.AGENT_ADMIN;

    await this.agentRepository.save(nextAdminAgent);
  }

  async addAgentToShop(shopDomain: string): Promise<any> {

  }

  getShopAgents(shopDomain: string): Promise<Agent[]> {
    return this.agentRepository.createQueryBuilder('agent')
      .leftJoinAndSelect('agent.shop', 'shop')
      .where('shop.domain = :shopDomain', { shopDomain })
      .getMany();
  }

  getAgentByShop(shopDomain: string): Promise<Agent> {
    return this.agentRepository.createQueryBuilder('agent')
      .leftJoinAndSelect('agent.shop', 'shop')
      .where('shop.domain = :shopDomain', { shopDomain })
      .getOne();
  }

  getShopAgentByUsername(shopDomain: string, username: string): Promise<Agent> {
    return this.agentRepository.createQueryBuilder('agent')
      .leftJoinAndSelect('agent.shop', 'shop')
      .leftJoinAndSelect('agent.info', 'info')
      .where('agent.username = :username', { username })
      .getOne();
  }

  getShopAgentByUsernameWithPass(shopDomain: string, username: string): Promise<Agent> {
    return this.agentRepository.createQueryBuilder('agent')
      .addSelect('agent.password')
      .leftJoinAndSelect('agent.shop', 'shop')
      .leftJoinAndSelect('agent.info', 'info')
      .where('agent.username = :username', { username })
      .andWhere('shop.domain = :shopDomain', { shopDomain })
      .getOne();
  }

  getShopAgentById(shopDomain: string, id: number): Promise<Agent> {
    return this.agentRepository.createQueryBuilder('agent')
      .leftJoinAndSelect('agent.info', 'info')
      .leftJoinAndSelect('agent.shop', 'shop')
      .where('agent.id = :id', { id })
      .andWhere('shop.domain = :shopDomain', { shopDomain })
      .getOne();
  }

  getShopAgentByIdWithPass(shopDomain: string, id: number): Promise<Agent> {
    return this.agentRepository.createQueryBuilder('agent')
      .addSelect('agent.password')
      .leftJoinAndSelect('agent.shop', 'shop')
      .leftJoinAndSelect('agent.info', 'info')
      .where('agent.id = :id', { id })
      .andWhere('shop.domain = :shopDomain', { shopDomain })
      .getOne();
  }

  private generatePassword(length: number) {
    const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let retVal = '';
    for (let i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }

}