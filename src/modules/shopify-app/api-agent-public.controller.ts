import { Body, Controller, Logger, Post, UnauthorizedException } from '@nestjs/common';
import { JwtAuthService } from './auth/jwt-auth.service';

@Controller('api/agent')
export class ApiAgentPublicController {
  constructor(
    private readonly authService: JwtAuthService,
    private readonly logger: Logger,
  ) { }

  @Post('login')
  async login(@Body() loginData: LoginDataDTO) {
    this.logger.log('Agent auth attempt: ' + loginData.username + '. shop: ' + loginData.shop);

    try {
      const agent = await this.authService
        .validateAgentCreds(loginData.username, loginData.password, loginData.shop);
      if (agent) {
        const expiresIn = 60 * 60 * 24 * 30 * 48;
        const token = await this.authService.issueToken(agent, loginData.shop, expiresIn);

        return { token, expires_in: expiresIn };
      } else return {};

    } catch (err) {
      this.logger.warn('Agent failed auth: ' + err);
      throw new UnauthorizedException('Credenciales invalidas');
    }
  }
}

export interface LoginDataDTO {
  readonly shop: string;
  readonly username: string;
  readonly password: string;
}
