import { Middleware, NestMiddleware, Logger, UnauthorizedException } from '@nestjs/common';
import { JwtAuthService } from './jwt-auth.service';

@Middleware()
export class JwtAuthMiddleware implements NestMiddleware {
  constructor(
    private readonly jwtAuthService: JwtAuthService,
  ) {
  }

  resolve(): (req, res, next) => void {
    return async (req, res, next) => {
      const authHeader = req.get('Authorization');
      if (!authHeader) throw new UnauthorizedException();
      const jwtToken = authHeader.replace('Bearer ', '');
      const jwtPayload = await this.jwtAuthService.getTokenPayload(jwtToken);
      const agent = await this.jwtAuthService.getPayloadAssociatedAgent(jwtPayload);
      const customer = await this.jwtAuthService.getPayloadAssociatedCustomer(jwtPayload);

      const user = agent || customer;

      if (user) {
        req.appUser = user;
      }

      next();
    };
  }
}
