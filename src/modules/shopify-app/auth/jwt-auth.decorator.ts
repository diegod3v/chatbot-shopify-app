import { ReflectMetadata } from '@nestjs/common';

export const JwtAuthProtected = () => ReflectMetadata('JwtAuthProtected', true);