import { createRouteParamDecorator } from '@nestjs/common';

export const AppUser = createRouteParamDecorator((data, req) => {
  return req.appUser;
});