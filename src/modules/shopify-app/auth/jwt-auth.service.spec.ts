import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import express from 'express';
import * as nock from 'nock';
import { JwtAuthService, InvalidUserCreds } from './jwt-auth.service';
import { AgentService } from '../agent/agent.service';
import * as bcrypt from 'bcrypt';
import { B_COST, JWT_SECRET } from '../../../config';
import { AgentRoles } from '../agent/AgentRolesEnum';
import * as jwt from 'jsonwebtoken';
import { Agent } from '../agent/Agent';

const mockUsername = 'johndoe';
const mockPass = 'abc123';
const mockShop = 'fakeshop.myfakefy.com';
const mockAgent: Agent = {
  id: 1,
  username: mockUsername,
  role: AgentRoles.AGENT_ADMIN,
  info: {
    job: null,
    avatarSrc: null,
    name: 'John Doe',
  },
  shop: null,
};

const mockAgentWithPass = { ...mockAgent, password: bcrypt.hashSync(mockPass, B_COST) };

const mockAgentServiceClazz = jest.fn(() => {
  return {
    getShopAgentByUsername: jest.fn(() => mockAgent),
    getShopAgentByUsernameWithPass: jest.fn(() => mockAgentWithPass),
  };
});

const mockAgentService = new mockAgentServiceClazz();

describe('JwtAuthService', () => {
  let module: TestingModule;

  const server = express();

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect('127.0.0.1');

    module = await Test.createTestingModule({
      components: [
        JwtAuthService,
        {
          provide: AgentService,
          useValue: mockAgentService,
        },
      ],
    }).compile();

    (server as any).enable('trust proxy');
    const app = module.createNestApplication(server);
    await app.init();
  });

  let service: JwtAuthService;
  beforeEach(() => {
    service = module.get(JwtAuthService);

    mockAgentService.getShopAgentByUsername.mockClear();
    mockAgentService.getShopAgentByUsernameWithPass.mockClear();
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should exist', () => {
    expect(service).toBeTruthy();
  });

  it('should validate mock user creds', async () => {

    const agent = await service.validateUserCreds(mockUsername, mockPass, mockShop);

    expect(agent).toBeTruthy();
    expect(agent.username).toBe(mockUsername);
  });

  it('should return error on validate mock user with invalid creds', async () => {

    try {
      const agent = await service.validateUserCreds(mockUsername, 'notthepass', mockShop);
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidUserCreds);
    }

  });

  it('should issue token', async () => {
    const token = await service.issueToken(mockAgent, mockShop, 60 * 60);

    const isValidToken = jwt.verify(token, JWT_SECRET);
    const tokenPayload = jwt.decode(token);

    expect(token).toBeTruthy();
    expect(isValidToken).toBeTruthy();
    expect(tokenPayload.username).toBe(mockUsername);
    expect(tokenPayload.shop).toBe(mockShop);
  });

  it('should validate the existence of mock user', async () => {
    const isValidUser = await service.getPayloadAssociatedUser({
      sub: mockUsername,
      username: mockUsername,
      role: AgentRoles.AGENT_ADMIN,
      shop: mockShop,
    });

    expect(isValidUser).toBe(true);
    expect(mockAgentService.getShopAgentByUsername).toBeCalledWith(mockShop, mockUsername);
  });
});
