import { CanActivate, ExecutionContext, Guard } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs/Observable';

@Guard()
export class JwtAuthGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) { }

  canActivate(req, context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const { parent, handler } = context;
    const isProtected = this.reflector.get<string[]>('JwtAuthProtected', handler);
    if (!isProtected) {
      return true;
    }

    const user = req.appUser;
    const isUser = !!(user && user.username);
    return isUser;
  }
}