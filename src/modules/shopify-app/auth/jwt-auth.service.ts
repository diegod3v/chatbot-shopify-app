import { Component } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../../../config';
import { Agent } from '../agent/Agent';
import { AgentService } from '../agent/agent.service';
import { CustomerService } from '../customer/customer.service';
import { Customer } from '../customer/Customer';

const JWT_ISSUER = 'ChatbotProj';

@Component()
export class JwtAuthService {
  constructor(
    private readonly agentService: AgentService,
    private readonly customerService: CustomerService,
  ) { }

  async validateAgentCreds(username: string, password: string, shop: string): Promise<Agent> {

    const agent = await this.agentService.getShopAgentByUsernameWithPass(shop, username);

    if (agent) {
      const isValidPass = await bcrypt.compare(password, agent.password);
      if (isValidPass)
        return agent;
      else throw new InvalidUserCreds('username: ' + username);
    } else {
      throw new InvalidUsername('username: ' + username);
    }
  }

  async issueToken(agent: Agent, shopDomain: string, expiresIn: number = 43200): Promise<any> {
    const payload = {
      sub: agent.username,
      username: agent.username,
      name: agent.info.name,
      shop: shopDomain,
      role: agent.role,
    };

    const token = new Promise((res, rej) => {
      jwt.sign(payload, JWT_SECRET, { expiresIn, issuer: JWT_ISSUER }, (err, signedToken) => {
        if (err) {
          rej(err);
        } else {
          res(signedToken);
        }
      });
    });

    return token;
  }

  async verifyTokenAndGetAgent(token: string): Promise<Agent> {
    const payload = await this.getTokenPayload(token);
    return this.getPayloadAssociatedAgent(payload);
  }

  async validateAgentXmppCreds(username: string, shop: string, token: string): Promise<Agent> {
    const agent = await this.verifyTokenAndGetAgent(token);

    if (agent && agent.username === username && agent.shop.domain === shop) {
      return agent;
    } else {
      return null;
    }
  }

  async getPayloadAssociatedAgent(jwtPayload: any): Promise<Agent> {
    const agentUsername = jwtPayload.sub;
    const agentShop = jwtPayload.shop;
    const agent = await this.agentService.getShopAgentByUsername(agentShop, agentUsername);
    if (agent) {
      return agent;
    } else {
      return null;
    }
  }

  async getPayloadAssociatedCustomer(jwtPayload: any): Promise<Customer> {
    const customerUsername = jwtPayload.sub;
    const customer = await this.customerService.getCustomerByUsername(customerUsername);
    if (customer) {
      return customer;
    } else {
      return null;
    }
  }

  async getTokenPayload(token: string): Promise<any> {
    return new Promise((res, rej) => {
      jwt.verify(token, JWT_SECRET, { issuer: JWT_ISSUER }, (err, decoded) => {
        if (err || !decoded) { rej((err || new Error('No Decoded JWT Data'))); }
        res(decoded);
      })
    })
  }
}

export class InvalidUserCreds extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, InvalidUserCreds.prototype);
    this.name = 'InvalidUserCreds';
  }
}

export class InvalidUsername extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, InvalidUsername.prototype);
    this.name = 'InvalidUsername';
  }
}