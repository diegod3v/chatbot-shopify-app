import { Body, Controller, Delete, Get, Param, Patch, Post, UnauthorizedException, UseGuards, Query } from '@nestjs/common';
import { Agent } from './agent/Agent';
import { AppUser } from './auth/app-user.decorator';
import { JwtAuthProtected } from './auth/jwt-auth.decorator';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { FlowConfigService } from './flow-config/flow-config.service';
import { FlowNodeDTO } from './flow-config/FlowNode.dto';
import { NodeOptionDTO } from './flow-config/NodeOption.dto';
import { omit } from 'lodash';
import { AgentService } from './agent/agent.service';
import { CustomerService } from './customer/customer.service';
import { ChatConfigService } from './chat-config/chat-config.service';
import { ChatConfigDTO } from './chat-config/chat-config.dto';
import { NlpConfigService } from './nlp-config/nlp-config.service';

@Controller('api/agent')
@UseGuards(JwtAuthGuard)
export class ApiAgentController {
  constructor(
    private readonly flowConfigService: FlowConfigService,
    private readonly customerService: CustomerService,
    private readonly chatConfigService: ChatConfigService,
    private readonly nlpConfigService: NlpConfigService,
  ) { }

  @Get('me')
  @JwtAuthProtected()
  me(@AppUser() user: Agent) {
    return user;
  }

  @Get('config')
  @JwtAuthProtected()
  async getChatConfig(@AppUser() user: Agent) {
    this.chatConfigService.setShopDomain(user.shop.domain);
    const config = await this.chatConfigService.getConfig();
    this.chatConfigService.clearShopDomain();
    return { ...config, id: undefined };
  }

  @Patch('config/:id')
  @JwtAuthProtected()
  async updateChatConfig(@AppUser() user: Agent, @Body() nextChatConfig: ChatConfigDTO) {
    this.chatConfigService.setShopDomain(user.shop.domain);

    const config = await this.chatConfigService.updateConfig(nextChatConfig);
    this.chatConfigService.clearShopDomain();
    return { ...config, id: undefined };
  }

  @Get('intents')
  @JwtAuthProtected()
  async getNlpIntents(@AppUser() user: Agent) {
    const intents = await this.nlpConfigService.getIntentList();
    return { intents };
  }

  @Get('customer')
  @JwtAuthProtected()
  async getCustomerInfoByUsername(@AppUser() user: Agent, @Query('username') customerUsername: string) {
    const customerRaw = await this.customerService.getCustomerByUsername(customerUsername);
    const customer = omit(customerRaw, ['shop']);
    return customer;
  }

  @Get('flow')
  @JwtAuthProtected()
  async getMyFlowConfig(@AppUser() user: Agent) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const config = await this.flowConfigService.getConfigForShop();
    this.flowConfigService.clearShopDomain();
    return { ...config, id: undefined };
  }

  @Get('flow/node')
  @JwtAuthProtected()
  async getAllFlowNodes(@AppUser() user: Agent) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const nextNode = await this.flowConfigService.getNodesForShop();
    this.flowConfigService.clearShopDomain();
    return nextNode;
  }

  @Get('flow/node/:id')
  @JwtAuthProtected()
  async getFlowNode(@AppUser() user: Agent, @Param('id') nodeId) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const nextNode = await this.flowConfigService.getNodeById(nodeId);
    this.flowConfigService.clearShopDomain();
    return nextNode;
  }

  @Post('flow/node')
  @JwtAuthProtected()
  async createFlowNode(@AppUser() user: Agent, @Body() node: FlowNodeDTO) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const nextNode = await this.flowConfigService.addNode(node);
    this.flowConfigService.clearShopDomain();
    return nextNode;
  }

  @Patch('flow/node/:id')
  @JwtAuthProtected()
  async updateFlowNode(@AppUser() user: Agent, @Param('id') nodeId, @Body() nextNode: FlowNodeDTO) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const updatedNode = await this.flowConfigService.updateNode(nodeId, nextNode);
    this.flowConfigService.clearShopDomain();
    return updatedNode;
  }

  @Delete('flow/node/:id')
  @JwtAuthProtected()
  async deleteFlowNode(@AppUser() user: Agent, @Param('id') nodeId) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const nextFlowConfig = await this.flowConfigService.deleteNode(nodeId);
    this.flowConfigService.clearShopDomain();
    return nextFlowConfig;
  }

  @Post('flow/option')
  async createOption(@AppUser() user: Agent, @Body() option: NodeOptionDTO) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const createdOption = await this.flowConfigService.createNodeOption(option);
    this.flowConfigService.clearShopDomain();
    if (!createdOption) {
      throw new UnauthorizedException();
    }
    return createdOption;
  }

  @Post('flow/node/:nodeId/option')
  async createOptionInNode(@AppUser() user: Agent, @Param('nodeId') nodeId, @Body() option: NodeOptionDTO) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const createdOption = await this.flowConfigService.createNodeOption({ ...option, flowNode: nodeId });
    this.flowConfigService.clearShopDomain();
    if (!createdOption) {
      throw new UnauthorizedException();
    }
    return createdOption;
  }

  @Patch('flow/option/:id')
  async updateOption(@AppUser() user: Agent, @Param('id') optionId, @Body() option: NodeOptionDTO) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const updatedOption = await this.flowConfigService.updateNodeOption(optionId, option);
    this.flowConfigService.clearShopDomain();
    if (!updatedOption) {
      throw new UnauthorizedException();
    }
    return updatedOption;
  }

  @Delete('flow/option/:id')
  async deleteOption(@AppUser() user: Agent, @Param('id') optionId) {
    this.flowConfigService.setShopDomain(user.shop.domain);
    const nextFlowNode = await this.flowConfigService.deleteNodeOption(optionId);
    this.flowConfigService.clearShopDomain();
    return nextFlowNode;
  }
}
