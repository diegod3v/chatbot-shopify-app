import { Component } from '@nestjs/common';
import { AuthStore } from './AuthStore';
import { ShopData } from './ShopData.entity';

@Component()
export class ShopifyStoreService implements AuthStore {
  constructor() { }

  storeShop(shop: ShopData): Promise<number> {
    throw new Error('Method not implemented.');
  }
  getShop(shopId: number): Promise<ShopData> {
    throw new Error('Method not implemented.');
  }
  getShopByDomain(shopDomain: string): Promise<ShopData> {
    throw new Error('Method not implemented.');
  }
  deleteShop(shopDomain: string) {
    throw new Error('Method not implemented.');
  }
}
