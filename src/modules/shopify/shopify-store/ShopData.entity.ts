import { IShopifyUser } from '../shopify-auth/ShopifyUser';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('Shop')
export class ShopData implements IShopifyUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 33 })
    accessToken: string;

    @Column()
    domain: string;
}