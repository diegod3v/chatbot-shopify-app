import { Component } from '@nestjs/common';
import { AuthStore } from './AuthStore';
import { ShopData } from './ShopData.entity';
import { ShopifyStoreService } from './shopify-store.service';
import MemoryStore from '../MemoryStore';

@Component()
export class ShopifyMemoryStoreService extends ShopifyStoreService {
  private memoryStore: ShopData[];

  constructor() {
    super();
    this.memoryStore = MemoryStore.UserTable;
  }

  storeShop(shop: ShopData): Promise<number> {
    shop.id = this.memoryStore.length;
    this.memoryStore.push(shop);
    return new Promise((res) => res(shop.id));
  }
  getShop(shopId: number): Promise<ShopData> {
    return new Promise((res, rej) => {
      res(this.memoryStore.find((shop: ShopData) => shop.id === shopId));
    });
  }
  getShopByDomain(shopDomain: string): Promise<ShopData> {
    return new Promise((res, rej) => {
      res(this.memoryStore.find((shop: ShopData) => shop.domain === shopDomain));
    });
  }
}
