import { Test } from '@nestjs/testing';
import { TestingModule } from '@nestjs/testing/testing-module';
import { expect } from 'chai';
import { ShopData } from './ShopData.entity';
import { ShopifyMemoryStoreService } from './shopify-memory-store.service';
import { ShopifyStoreService } from './shopify-store.service';

describe('ShopifyStoreService with MemoryStore implementation', () => {
    let module: TestingModule;
    beforeEach(() => {
        return Test.createTestingModule({
            components: [
                {
                    provide: ShopifyStoreService,
                    useClass: ShopifyMemoryStoreService,
                },
            ],
        }).compile()
            .then(compiledModule => module = compiledModule);
    });

    let service: ShopifyStoreService;
    beforeEach(() => {
        service = module.get(ShopifyStoreService);
    });

    it('should exist', () => {
        expect(service).to.exist;
    });

    it('should save and return info of a shop', async () => {
        const shop = new ShopData();
        shop.accessToken = 'e407634c3f9649cb95a4650bca2a2a8f1';
        shop.domain = 'atomsstore.myshopify.com';

        await service.storeShop(shop);
        const retrievedShop = await service.getShopByDomain(shop.domain);

        expect(retrievedShop.domain).to.equal(shop.domain);
        expect(retrievedShop.accessToken).to.equal(shop.accessToken);
    });
});
