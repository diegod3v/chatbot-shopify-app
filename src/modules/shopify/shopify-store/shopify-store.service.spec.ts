import { Test } from '@nestjs/testing';
import { TestingModule } from '@nestjs/testing/testing-module';
import { ShopifyStoreService } from './shopify-store.service';
import { expect } from 'chai';

describe('ShopifyStoreService', () => {
  let module: TestingModule;
  beforeEach(() => {
    return Test.createTestingModule({
      components: [
        ShopifyStoreService,
      ],
    }).compile()
      .then(compiledModule => module = compiledModule);
  });

  let service: ShopifyStoreService;
  beforeEach(() => {
    service = module.get(ShopifyStoreService);
  });

  it('should exist', () => {
    expect(service).to.exist;
  });
});
