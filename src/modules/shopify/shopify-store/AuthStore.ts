import { ShopData } from './ShopData.entity';

export interface AuthStore {
    storeShop(shop: ShopData): Promise<number>;
    getShop(shopId: number): Promise<ShopData>;
    getShopByDomain(shopDomain: string): Promise<ShopData>;
    deleteShop(shopDomain: string);
}