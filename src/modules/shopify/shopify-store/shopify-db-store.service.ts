import { Component } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthStore } from './AuthStore';
import { ShopData } from './ShopData.entity';
import { ShopifyStoreService } from './shopify-store.service';
import { Repository } from 'typeorm';

@Component()
export class ShopifyDBStoreService extends ShopifyStoreService {
  constructor(@InjectRepository(ShopData) private readonly shopDataRepository: Repository<ShopData>) {
    super();
  }

  storeShop(shop: ShopData): Promise<number> {
    return this.shopDataRepository.save(shop).then((shopData) => shopData.id);
  }
  getShop(shopId: number): Promise<ShopData> {
    return this.shopDataRepository.findOne(shopId);
  }
  getShopByDomain(shopDomain: string): Promise<ShopData> {
    return this.shopDataRepository.findOne({ domain: shopDomain });
  }
  async deleteShop(shopDomain: string) {
    const storedShop = await this.getShopByDomain(shopDomain);
    return storedShop ? this.shopDataRepository.remove((storedShop as ShopData)) : undefined;
  }
}
