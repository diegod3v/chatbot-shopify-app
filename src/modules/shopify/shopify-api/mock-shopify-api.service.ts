/* import { Component } from '@nestjs/common';
import { ShopifyAuthConfig } from '../shopify-auth-utils/shopify-auth-config.service';
import { ShopifyApiService } from './shopify-api.service';
import { WebhookTopic, IShop, IProduct } from 'shopify-api-node';

@Component()
export class MockShopifyApiService extends ShopifyApiService {
  async createScriptTag(src: any): Promise<any> {
    return '';
  }

  async createWebhook(topic: WebhookTopic, address: string, format: 'json' | 'xml' = 'json'): Promise<any> {
    return '';
  }

  async getShopInfo(): Promise<IShop> {
    return {
      address1: 'foobar',
      address2: 'foobar',
      city: 'foobar',
      country: 'foobar',
      country_code: 'foobar',
      country_name: 'foobar',
      created_at: 'foobar',
      county_taxes: 'foobar',
      customer_email: 'foobar',
      currency: 'foobar',
      domain: 'foobar',
      eligible_for_card_reader_giveaway: true,
      eligible_for_payments: true,
      email: 'foobar',
      finances: true,
      force_ssl: true,
      google_apps_domain: 'foobar',
      google_apps_login_enabled: '',
      has_discounts: true,
      has_gift_cards: true,
      has_storefront: true,
      iana_timezone: 'foobar',
      id: 123456,
      latitude: 123456,
      longitude: 123456,
      money_format: 'foobar',
      money_in_emails_format: 'foobar',
      money_with_currency_format: 'foobar',
      money_with_currency_in_emails_format: 'foobar',
      myshopify_domain: 'foobar',
      name: 'foobar',
      password_enabled: true,
      phone: 'foobar',
      plan_display_name: 'foobar',
      plan_name: 'foobar',
      primary_locale: 'foobar',
      primary_location_id: 123456,
      province: 'foobar',
      province_code: 'foobar',
      requires_extra_payments_agreement: true,
      setup_required: true,
      shop_owner: 'foobar',
      source: 'foobar',
      tax_shipping: true,
      taxes_included: true,
      timezone: 'foobar',
      updated_at: 'foobar',
      weight_unit: 'foobar',
      zip: 'foobar',
    };
  }

  async getProductsByTitle(title: string): Promise<IProduct[]> {
    return Promise.all([
      this.getOneProductByTitle(''),
      this.getOneProductByTitle(''),
      this.getOneProductByTitle(''),
      this.getOneProductByTitle(''),
    ]);
  }

  async getOneProductByTitle(title: string): Promise<IProduct> {
    return {
      body_html: 'Foo bar',
      created_at: 'Foo bar',
      handle: 'foo-bar',
      id: 12356,
      image: 'any',
      images: null,
      options: null,
      product_type: 'Foo bar',
      published_at: 'Foo bar',
      published_scope: 'Foo bar',
      tags: 'Foo bar',
      template_suffix: 'Foo bar',
      title: 'Foo bar',
      updated_at: 'Foo bar',
      variants: null,
    };
  }
}
 */