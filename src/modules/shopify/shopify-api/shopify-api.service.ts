import { Component } from '@nestjs/common';
import { WebhookTopic, IShop, IProduct } from 'shopify-api-node';

@Component()
export class ShopifyApiService {
  public shop: string;
  public accessToken: string;

  createScriptTag(src: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  createWebhook(topic: WebhookTopic, address: string, format?: 'json' | 'xml'): Promise<any> {
    throw new Error('Method not implemented.');
  }

  getShopInfo(): Promise<IShop> {
    throw new Error('Method not implemented.');
  }

  getProductsByTitle(title: string): Promise<IProduct[]> {
    throw new Error('Method not implemented.');
  }

  async getOneProductByTitle(title: string): Promise<IProduct> {
    throw new Error('Method not implemented.');
  }
}
