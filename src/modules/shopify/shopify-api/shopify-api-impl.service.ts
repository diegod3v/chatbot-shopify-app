import { Component } from '@nestjs/common';
import { ShopifyAuthConfig } from '../shopify-auth-utils/shopify-auth-config.service';
import * as Shopify from 'shopify-api-node';
import { ShopifyApiService } from './shopify-api.service';
import { WebhookTopic } from 'shopify-api-node';

@Component()
export class ShopifyApiServiceImpl extends ShopifyApiService {
  get api(): Shopify {
    return new Shopify({ shopName: this.shop, accessToken: this.accessToken });
  }

  createScriptTag(src: any): Promise<any> {
    return this.api.scriptTag.create({ src, event: 'onload' });
  }

  createWebhook(topic: WebhookTopic, address: string, format: 'json' | 'xml' = 'json'): Promise<any> {
    return this.api.webhook.create({ topic, address, format });
  }

  getShopInfo(): Promise<Shopify.IShop> {
    return this.api.shop.get();
  }

  getProductsByTitle(title: string): Promise<Shopify.IProduct[]> {
    return this.api.product.list({ title });
  }

  async getOneProductByTitle(title: string): Promise<Shopify.IProduct> {
    const products = this.api.product.list({ title });
    return products[0];
  }
}
