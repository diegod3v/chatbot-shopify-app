import { Component } from '@nestjs/common';
import * as shopifyApi from 'shopify-node-api';
import { ShopifyUtils } from './ShopifyUtils';
import * as Crypto from 'crypto';
import { IShop } from './IShop';

@Component()
export class ShopifyUtilsService implements ShopifyUtils {
  constructor() {
  }

  fetchShopInfo(shopDomain: string, acccesToken: string): Promise<IShop> {
    const Shopify = new shopifyApi({
      shop: shopDomain,
      access_token: acccesToken,
    });

    return new Promise((res, rej) => {
      Shopify.get('/admin/shop.json', null, (err, data, headers) => {
        if (err) rej(err);
        else res(data.shop);
      });
    });
  }

  getCurrentThemeId(shopDomain: string, acccesToken: string): Promise<number> {
    const Shopify = new shopifyApi({
      shop: shopDomain,
      access_token: acccesToken,
    });

    return new Promise((res, rej) => {
      Shopify.get('/admin/themes.json', null, (err, data, headers) => {
        if (err) rej(err);

        const themes: any[] = data.themes;
        const currentTheme = themes.find((theme) => theme.role === 'main');

        if (currentTheme) res(currentTheme.id);
        else rej(new Error('No theme found'));
      });
    });
  }
}
