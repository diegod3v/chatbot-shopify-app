import { Test } from '@nestjs/testing';
import { TestingModule } from '@nestjs/testing/testing-module';
import { ShopifyUtilsService } from './shopify-utils.service';
import { expect } from 'chai';
import * as Crypto from 'crypto';

describe('ShopifyUtilsService', () => {
  let module: TestingModule;

  beforeEach(() => {
    return Test.createTestingModule({
      components: [
        ShopifyUtilsService,
      ],
    }).compile()
      .then(compiledModule => module = compiledModule);
  });

  let service: ShopifyUtilsService;
  beforeEach(() => {
    service = module.get(ShopifyUtilsService);
  });

  it('should exist', () => {
    expect(service).to.exist;
  });
});
