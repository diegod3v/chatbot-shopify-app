import { IShop } from './IShop';

export interface ShopifyUtils {
    fetchShopInfo(shopDomain: string, acccesToken: string): Promise<IShop>;
    getCurrentThemeId(shopDomain: string, acccesToken: string): Promise<number>;
}