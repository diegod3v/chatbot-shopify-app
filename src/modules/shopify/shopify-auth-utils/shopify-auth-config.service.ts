import { Component } from '@nestjs/common';
import { ShopifyAuthUtils } from './ShopifyAuthUtils';

@Component()
export class ShopifyAuthConfig {
    public publicApiKey: string;
    public apiSecretKey: string;
    public redirectUri: string;
    public appScope: string;

    constructor(publicApiKey: string, apiSecretKey: string, redirectUri: string, appScope: string) {
        this.publicApiKey = publicApiKey;
        this.apiSecretKey = apiSecretKey;
        this.redirectUri = redirectUri;
        this.appScope = appScope;
    }
}
