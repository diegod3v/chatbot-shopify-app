import { Test } from '@nestjs/testing';
import { TestingModule } from '@nestjs/testing/testing-module';
import { expect } from 'chai';
import * as Crypto from 'crypto';
import { ShopifyAuthUtilsService } from '../shopify-auth-utils/shopify-auth-utils.service';
import { ShopifyAuthConfig } from './shopify-auth-config.service';

describe('ShopifyAuthUtilsService', () => {
  let module: TestingModule;

  const mockPublicApiKey = 'f304ad7e23440faa4f29ab99b23948572';
  const mockApiSecretKey = 'e407634c3f9649cb95a4650bca2a2a8f1';
  const mockRedirectUrl = 'http://localhost:8080/';
  const mockAppScope = 'read_themes,write_themes,read_themes,write_themes,read_fulfillments,write_fulfillments';

  beforeEach(() => {
    return Test.createTestingModule({
      components: [
        {
          provide: ShopifyAuthConfig,
          useValue: new ShopifyAuthConfig(
            mockPublicApiKey,
            mockApiSecretKey,
            mockRedirectUrl,
            mockAppScope,
          ),
        },
        ShopifyAuthUtilsService,
      ],
    }).compile()
      .then(compiledModule => module = compiledModule);
  });

  let service: ShopifyAuthUtilsService;
  beforeEach(() => {
    service = module.get(ShopifyAuthUtilsService);
  });

  it('should exist', () => {
    expect(service).to.exist;
  });

  it('should return valid auth url (installation url)', () => {
    const shopDomain = 'atomsstore.myshopify.com';
    const nonce = 'mYr4nd0mN0nc3';

    const authUrl = service.getAuthUrl(shopDomain, nonce);

    expect(authUrl).to.exist;
    expect(authUrl).not.to.be.empty;
    expect(authUrl).to.have.string(shopDomain);
    expect(authUrl).to.have.string(('state=' + nonce));
    expect(authUrl).to.have.string(('redirect_uri=' + mockRedirectUrl));
    expect(authUrl).to.have.string(('client_id=' + mockPublicApiKey));

  });

  it('should validate a signature (HMAC)', () => {
    const nonce = 'mYr4nd0mN0nc3';
    const mockQueryParams = {
      code: '0907a61c0c8d55e99db179b68161bc00',
      shop: 'atomsstore.myshopify.com',
      state: nonce,
      timestamp: '1337178173',
    };

    const queryString = Object.keys(mockQueryParams).map((key, i) => {
      return key + '=' + mockQueryParams[key];
    }).reduce((prev, curr, i) => prev + '&' + curr);

    const mockHmac = Crypto.createHmac('sha256', mockApiSecretKey)
      .update(queryString)
      .digest('hex');

    (mockQueryParams as any).hmac = mockHmac;

    const validSignature = service.isValidSignature(mockQueryParams, nonce);

    expect(validSignature).to.exist;
    expect(validSignature).is.true;
  });

  it('should generate a valid nonce', () => {
    const mockAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36';
    const mockIp = '127.0.0.0';
    const mockTimestamp = new Date();

    const generatedNonce = service.generateNonce(mockAgent, mockIp, mockTimestamp.getTime().toString());
    const secondGeneratedNonce = service.generateNonce(mockAgent, mockIp, mockTimestamp.getTime().toString());

    expect(generatedNonce).to.exist;
    expect(generatedNonce).not.to.be.empty;
    expect(generatedNonce).to.equal(secondGeneratedNonce);
  });

});
