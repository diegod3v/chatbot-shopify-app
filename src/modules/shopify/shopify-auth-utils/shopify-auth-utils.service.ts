import { Component } from '@nestjs/common';
import * as shopifyApi from 'shopify-node-api';
import * as Crypto from 'crypto';
import { ShopifyAuthUtils } from './ShopifyAuthUtils';
import { ShopifyAuthConfig } from './shopify-auth-config.service';
import * as nonce from 'nonce';
import { omit } from 'lodash';
import * as querystring from 'querystring';
const nonceGenerator = nonce();

@Component()
export class ShopifyAuthUtilsService implements ShopifyAuthUtils {
  private publicApiKey: string;
  private apiSecretKey: string;
  private redirectUri: string;
  private appScope: string;

  constructor(private readonly shopifyAuthConfig: ShopifyAuthConfig) {
    this.publicApiKey = shopifyAuthConfig.publicApiKey;
    this.apiSecretKey = shopifyAuthConfig.apiSecretKey;
    this.redirectUri = shopifyAuthConfig.redirectUri;
    this.appScope = shopifyAuthConfig.appScope;
  }

  exchangeCodeForAccessToken(shopDomain: string, code: string): Promise<string> {
    const Shopify = new shopifyApi({
      shop: shopDomain,
      shopify_api_key: this.publicApiKey,
      shopify_shared_secret: this.apiSecretKey,
    });

    const data = {
      client_id: this.publicApiKey,
      client_secret: this.apiSecretKey,
      code,
    };

    return new Promise((res, rej) => {
      Shopify.makeRequest('/admin/oauth/access_token', 'POST', data, (err, body) => {

        if (err) {
          rej(err);
        }

        res(body.access_token);
      });
    });
  }

  getAuthUrl(shopDomain: string, nonce: string): string {
    const Shopify = new shopifyApi({
      shop: shopDomain,
      shopify_api_key: this.publicApiKey,
      shopify_shared_secret: this.apiSecretKey,
      shopify_scope: this.appScope,
      redirect_uri: this.redirectUri,
      nonce,
    });

    return Shopify.buildAuthURL();
  }

  isValidSignature(params: any, nonce: string): boolean {
    const { shop, hmac, code } = params;

    if (!shop || !hmac || !code)
      return false;

    let map = { ...params };
    map = omit(map, ['signature', 'hmac']);

    const message = querystring.stringify(map);
    const providedHmac = Buffer.from(hmac, 'utf-8');
    const generatedHash = Buffer.from(
      Crypto
        .createHmac('sha256', this.apiSecretKey)
        .update(message)
        .digest('hex'),
      'utf-8',
    );

    let hashEquals = false;
    try {
      hashEquals = Crypto.timingSafeEqual(generatedHash, providedHmac)
    } catch (e) {
      hashEquals = false;
    };

    return hashEquals;
  }

  isValidBodySignature(body: any, hmac: string): boolean {
    const generatedHash = Crypto
      .createHmac('sha256', this.apiSecretKey)
      .update(body)
      .digest('base64');

    return generatedHash === hmac;
  }

  generateNonce(): string {
    return nonceGenerator();
  }
}
