export interface ShopifyAuthUtils {
    exchangeCodeForAccessToken(shopDomain: string, code: string): Promise<string>;
    getAuthUrl(shopDomain: string, nonce: string): string;
    isValidSignature(params: any, nonce: string): boolean;
    isValidBodySignature(body: any, hmac: string): boolean;
    generateNonce(): string;
}