import { Test } from '@nestjs/testing';
import { TestingModule } from '@nestjs/testing/testing-module';
import { expect } from 'chai';
import { ShopifyAuthConfig } from '../shopify-auth-utils/shopify-auth-config.service';

describe('ShopifyAuthUtilsService', () => {
  let module: TestingModule;

  const mockPublicApiKey = 'f304ad7e23440faa4f29ab99b23948572';
  const mockApiSecretKey = 'e407634c3f9649cb95a4650bca2a2a8f1';
  const mockRedirectUrl = 'http://localhost:8080/';
  const mockAppScope = 'read_themes,write_themes,read_themes,write_themes,read_fulfillments,write_fulfillments';

  beforeEach(() => {
    return Test.createTestingModule({
      components: [
        {
          provide: ShopifyAuthConfig,
          useValue: new ShopifyAuthConfig(
            mockPublicApiKey,
            mockApiSecretKey,
            mockRedirectUrl,
            mockAppScope,
          ),
        },
      ],
    }).compile()
      .then(compiledModule => module = compiledModule);
  });

  let service: ShopifyAuthConfig;
  beforeEach(() => {
    service = module.get(ShopifyAuthConfig);
  });

  it('should exist', () => {
    expect(service).to.exist;
  });

  it('should return set configuration', () => {

    expect(service.apiSecretKey).to.equal(mockApiSecretKey);
    expect(service.publicApiKey).to.equal(mockPublicApiKey);
    expect(service.redirectUri).to.equal(mockRedirectUrl);
    expect(service.appScope).to.equal(mockAppScope);
  });

});
