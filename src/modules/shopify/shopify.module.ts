import { DynamicModule, Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShopifyApiServiceImpl } from './shopify-api/shopify-api-impl.service';
import { ShopifyApiService } from './shopify-api/shopify-api.service';
import { ShopifyAuthConfig } from './shopify-auth-utils/shopify-auth-config.service';
import { ShopifyAuthUtilsService } from './shopify-auth-utils/shopify-auth-utils.service';
import { ShopifyAuthCallbacks } from './shopify-auth/shopify-auth-callbacks.service';
import { ShopifyAuthVoidCallbacks } from './shopify-auth/shopify-auth-void-callbacks.service';
import { ShopData } from './shopify-store/ShopData.entity';
import { ShopifyMemoryStoreService } from './shopify-store/shopify-memory-store.service';
import { ShopifyStoreService } from './shopify-store/shopify-store.service';
import { ShopifyUtilsService } from './shopify-utils/shopify-utils.service';

@Module({
  components: [
    Logger,
    ShopifyUtilsService,
  ],
  exports: [
    ShopifyUtilsService,
  ],
})
export class ShopifyModule {
  static forRoot(authOptions: ShopifyModuleAuthOptions, options: ShopifyModuleOptions = {}): DynamicModule {
    return {
      module: ShopifyModule,
      imports: [TypeOrmModule.forFeature([ShopData])],
      components: [
        {
          provide: ShopifyAuthConfig,
          useValue: new ShopifyAuthConfig(
            authOptions.apiKey,
            authOptions.secretKey,
            authOptions.redirectUrl,
            authOptions.permissions,
          ),
        },
        {
          provide: ShopifyAuthCallbacks,
          useClass: options.authCallbacksImpl || ShopifyAuthVoidCallbacks,
        },
        {
          provide: ShopifyStoreService,
          useClass: options.storeService || ShopifyMemoryStoreService,
        },
        {
          provide: ShopifyApiService,
          useClass: ShopifyApiServiceImpl,
        },
        ShopifyAuthUtilsService,
      ],
      exports: [ShopifyAuthUtilsService, ShopifyAuthCallbacks, ShopifyStoreService, ShopifyApiService],
    };
  }
}

export interface ShopifyModuleOptions {
  authCallbacksImpl?: any;
  storeService?: any;
}

export interface ShopifyModuleAuthOptions {
  apiKey: string;
  secretKey: string;
  redirectUrl: string;
  permissions: string;
}
