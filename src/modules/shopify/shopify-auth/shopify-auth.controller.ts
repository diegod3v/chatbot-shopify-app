import {
  Controller, ForbiddenException, Get, Headers, InternalServerErrorException,
  Logger, Post, Query, Req, Res, Session, UseGuards,
} from '@nestjs/common';
import { ShopifyApiService } from '../shopify-api/shopify-api.service';
import { ShopifyAuthUtilsService } from '../shopify-auth-utils/shopify-auth-utils.service';
import { ShopData } from '../shopify-store/ShopData.entity';
import { ShopifyStoreService } from '../shopify-store/shopify-store.service';
import { IShopifyUser } from './ShopifyUser';
import { ShopifyAuthCallbacks } from './shopify-auth-callbacks.service';
import { ShopifyProtected } from './shopify-auth.decorator';
import { ShopifyUserGuard } from './shopify-auth.guard';
import { ShopifyUser } from './shopify-user.decorator';
import { SHOPIFY_UNINSTALL_URL, SHOPIFY_UNINSTALL_ROUTE } from '../../../config';
import * as cookie from 'cookie';

@UseGuards(ShopifyUserGuard)
@Controller('shopify')
export class ShopifyAuthController {
  constructor(
    private readonly logger: Logger,
    private readonly shopifyService: ShopifyAuthUtilsService,
    private readonly shopifyStoreService: ShopifyStoreService,
    private readonly shopifyCallbacks: ShopifyAuthCallbacks,
    private readonly shopifyApi: ShopifyApiService,
  ) { }

  @Get('auth')
  getInstallUrl(@Query('shop') shopDomain, @Res() res, @Req() req, @Headers() headers): void {
    const nonce = this.shopifyService.generateNonce();
    const installUrl = this.shopifyService.getAuthUrl(shopDomain, nonce);

    this.logger.log('Sended nonce: ' + nonce);

    res.cookie('state', nonce);
    res.redirect(installUrl);
  }

  @Get('auth/callback')
  async installCallback(@Query() queryMap, @Query('shop') shop, @Query('code') code,
    @Res() res, @Req() req, @Headers('cookie') cookieHeader, @Session() session): Promise<any> {

    this.logger.log('Installation/Auth Attempt shop: ' + shop);
    this.logger.log('Installation/Auth Attempt ip:' + req.ip);

    const parsedCookieHeader = cookie.parse(cookieHeader);
    const nonce = parsedCookieHeader.state;

    this.logger.log('received nonce: ' + nonce);

    const isValidReq = this.shopifyService.isValidSignature(queryMap, nonce);

    if (isValidReq) {
      const alreadyStoredShop = await this.shopifyStoreService.getShopByDomain(shop);
      let shopId;
      let nextShop;

      if (!alreadyStoredShop) {
        try {
          await this.shopifyCallbacks.beforeInstall(shop);
          const accessToken = await this.shopifyService.exchangeCodeForAccessToken(shop, code);

          nextShop = new ShopData();
          nextShop.accessToken = accessToken;
          nextShop.domain = shop;
          shopId = await this.shopifyStoreService.storeShop(nextShop);
          this.shopifyApi.accessToken = accessToken;
          this.shopifyApi.shop = shop;
          await this.shopifyApi.createWebhook('app/uninstalled', SHOPIFY_UNINSTALL_URL);
        } catch (error) {
          this.logger.error(error);
          throw new ForbiddenException('Not Allowed Installation');
        }

        try {
          await this.shopifyCallbacks.afterInstall(nextShop);
        } catch (error) {
          this.logger.error(error);
          throw new InternalServerErrorException();
        }

        this.logger.log('Installed: ' + nextShop.domain);

      } else {
        nextShop = alreadyStoredShop;
        try {
          await this.shopifyCallbacks.beforeAuth(shop);
        } catch (error) {
          this.logger.warn(error);
          throw new ForbiddenException('Not Allowed Auth');
        }

        try {
          await this.shopifyCallbacks.afterAuth(nextShop);
        } catch (error) {
          this.logger.error(error);
          throw new InternalServerErrorException();
        }

        shopId = nextShop.id;
      }

      session && (session.shopId = shopId);
      this.logger.log('Logged: ' + shop);
    } else {
      this.logger.warn('Installation attempt fail, bad data');
      throw new ForbiddenException('Not Allowed Data. Reinstall');
    }

    res.redirect('/');
  }

  @ShopifyProtected()
  @Post(SHOPIFY_UNINSTALL_ROUTE)
  async uninstallCallback(@ShopifyUser() shopifyUser: IShopifyUser) {

    const shopDomain = shopifyUser.domain;

    this.logger.warn('Try App Uninstalled: ' + shopDomain);
    try {
      await this.shopifyCallbacks.beforeUninstall(shopDomain);
      const isStoredShop = await this.shopifyStoreService.getShopByDomain(shopDomain);

      if (isStoredShop) {
        await this.shopifyStoreService.deleteShop(shopDomain);
        await this.shopifyCallbacks.afterUninstall(shopDomain);
      } else throw new Error('Shop Not Found');
    } catch (error) {
      this.logger.error('Uninstall Error: ' + error);
    }

    return '';

  }
}
