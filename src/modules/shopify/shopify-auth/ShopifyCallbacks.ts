import { IShopifyUser } from './ShopifyUser';

export interface ShopifyCallbacks {
    beforeAuth(shop: string): any;
    beforeInstall(shop: string): any;
    afterInstall(shopifyUser: IShopifyUser): any;
    afterAuth(shopifyUser: IShopifyUser): any;
}