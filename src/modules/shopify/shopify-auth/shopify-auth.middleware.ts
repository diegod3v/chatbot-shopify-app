import { Middleware, NestMiddleware, Logger } from '@nestjs/common';
import * as getRawBody from 'raw-body';
import { ShopifyAuthUtilsService } from '../shopify-auth-utils/shopify-auth-utils.service';
import { ShopifyStoreService } from '../shopify-store/shopify-store.service';
import { ShopifyApiService } from '../shopify-api/shopify-api.service';

@Middleware()
export class ShopifyAuthMiddleware implements NestMiddleware {
  constructor(
    private readonly shopifyStoreService: ShopifyStoreService,
    private readonly shopifyAuthUtilsService: ShopifyAuthUtilsService,
    private readonly shopifyApi: ShopifyApiService,
    private readonly logger: Logger,
  ) {
  }

  resolve(): (req, res, next) => void {
    return async (req, res, next) => {
      const { session } = req;
      const hmac = req.get('X-Shopify-Hmac-Sha256');
      const topic = req.get('X-Shopify-Topic');
      const shopDomain = req.get('X-Shopify-Shop-Domain');
      const { body: data } = req;

      if (typeof session !== 'undefined' && typeof session.shopId !== 'undefined') {
        const user = await this.shopifyStoreService.getShop(session.shopId);
        if (typeof user !== 'undefined') {
          req.shopifyUser = user;
          this.shopifyApi.accessToken = user.accessToken;
          this.shopifyApi.shop = user.domain;
        } else {
          req.session = null;
          this.logger.log('Invalid session, deleted');
        }
      }
      else if (hmac && topic && shopDomain && data) {
        const rawBody = req.rawBody;
        const isValidHmac = this.shopifyAuthUtilsService.isValidBodySignature(rawBody, hmac);

        if (isValidHmac) {
          const user = await this.shopifyStoreService.getShopByDomain(shopDomain);
          req.shopifyUser = user;
          if (typeof user !== 'undefined') {
            this.shopifyApi.accessToken = user.accessToken;
            this.shopifyApi.shop = user.domain;
          }
        }
      }

      next();
    };
  }
}
