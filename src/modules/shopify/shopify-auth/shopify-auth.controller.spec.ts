import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { expect } from 'chai';
import * as Crypto from 'crypto';
import express from 'express';
import * as nock from 'nock';
import request from 'supertest';
import Shopify from 'shopify-api-node';
import { ShopifyApiServiceImpl } from '../shopify-api/shopify-api-impl.service';
import { ShopifyApiService } from '../shopify-api/shopify-api.service';
import { ShopifyAuthConfig } from '../shopify-auth-utils/shopify-auth-config.service';
import { ShopifyAuthUtilsService } from '../shopify-auth-utils/shopify-auth-utils.service';
import { ShopData } from '../shopify-store/ShopData.entity';
import { ShopifyDBStoreService } from '../shopify-store/shopify-db-store.service';
import { ShopifyStoreService } from '../shopify-store/shopify-store.service';
import { ShopifyAuthCallbacks } from './shopify-auth-callbacks.service';
import { ShopifyAuthVoidCallbacks } from './shopify-auth-void-callbacks.service';
import { ShopifyAuthController } from './shopify-auth.controller';

const mockShopifyApiService = jest.fn().mockImplementation(() => {
  return {
    createScriptTag: jest.fn(),
    createWebhook: jest.fn(),
  };
});

describe('ShopifyAuthController', () => {
  let module: TestingModule;

  const mockPublicApiKey = 'f304ad7e23440faa4f29ab99b23948572';
  const mockApiSecretKey = 'e407634c3f9649cb95a4650bca2a2a8f1';
  const mockRedirectUrl = 'http://localhost:8080/';
  const mockAppScope = 'read_themes,write_themes,read_themes,write_themes,read_fulfillments,write_fulfillments';

  const server = express();

  const mockShopAccessToken = 'f85632530bf277ec9ac6f649fc327f17';

  const shopDomain = 'atomsstore.myshopify.com';
  const userAgent = 'nodevice';
  const mockIp = '192.168.2.1';

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect('127.0.0.1');
    const shopifyAuthConfig = new ShopifyAuthConfig(
      mockPublicApiKey,
      mockApiSecretKey,
      mockRedirectUrl,
      mockAppScope,
    );

    const shopifyService = new ShopifyAuthUtilsService(shopifyAuthConfig);

    // mock shopify request
    shopifyService.exchangeCodeForAccessToken = () => {
      return new Promise((res) => res(mockShopAccessToken));
    };

    module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqljs',
          autoSave: false,
          synchronize: true,
          entities: [
            __dirname + '/../**/*.entity{.ts,.js}',
          ],
        }),
        TypeOrmModule.forFeature([ShopData]),
      ],
      controllers: [
        ShopifyAuthController,
      ],
      components: [
        Logger,
        {
          provide: ShopifyAuthUtilsService,
          useValue: shopifyService,
        },
        {
          provide: ShopifyStoreService,
          useClass: ShopifyDBStoreService,
        },
        {
          provide: ShopifyAuthCallbacks,
          useClass: ShopifyAuthVoidCallbacks,
        },
        {
          provide: ShopifyApiService,
          useClass: mockShopifyApiService,
        },
      ],
    }).compile();

    (server as any).enable('trust proxy');
    const app = module.createNestApplication(server);
    await app.init();
  });

  let controller: ShopifyAuthController;
  let shopifyService: ShopifyAuthUtilsService;
  let shopifyStoreService: ShopifyStoreService;
  beforeEach(() => {
    controller = module.get(ShopifyAuthController);
    shopifyService = module.get(ShopifyAuthUtilsService);
    shopifyStoreService = module.get(ShopifyStoreService);
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should exist', () => {
    expect(controller).to.exist;
  });

  it('should redirect to auth path', () => {

    const mockTimestamp = new Date();
    const generatedNonce = shopifyService.generateNonce(userAgent, mockIp, mockTimestamp.getTime().toString());

    const authRoute = shopifyService.getAuthUrl(shopDomain, generatedNonce);
    return request(server)
      .get('/shopify/auth?shop=' + shopDomain)
      .set('Remote-Addr', mockIp)
      .set('X-Forwarded-For', mockIp)
      .set('User-Agent', userAgent)
      .expect(302)
      .expect('Location', authRoute);
  });

  it('should store shop info on complete auth', async () => {
    const mockTimestamp = new Date();
    const generatedNonce = shopifyService.generateNonce(userAgent, mockIp, mockTimestamp.getTime().toString());
    const mockQueryParams = {
      code: '0907a61c0c8d55e99db179b68161bc00',
      shop: 'atomsstore.myshopify.com',
      state: generatedNonce,
      timestamp: '1337178173',
    };

    let queryString = Object.keys(mockQueryParams).map((key, i) => {
      return key + '=' + mockQueryParams[key];
    }).reduce((prev, curr, i) => prev + '&' + curr);

    const mockHmac = Crypto.createHmac('sha256', mockApiSecretKey)
      .update(queryString)
      .digest('hex');

    (mockQueryParams as any).hmac = mockHmac;

    queryString = Object.keys(mockQueryParams).map((key, i) => {
      return key + '=' + mockQueryParams[key];
    }).reduce((prev, curr, i) => prev + '&' + curr);

    await request(server)
      .get('/shopify/auth/callback?' + queryString)
      .set('Remote-Addr', mockIp)
      .set('X-Forwarded-For', mockIp)
      .set('User-Agent', userAgent)
      .expect(302)
      .expect('Location', '/');

    const storedShop = await shopifyStoreService.getShopByDomain(mockQueryParams.shop);

    expect(storedShop.domain).to.equal(mockQueryParams.shop);
    expect(storedShop.accessToken).to.equal(mockShopAccessToken);

  });

  it('should delete shop on uninstall', async () => {
    const mockTimestamp = new Date();
    const generatedNonce = shopifyService.generateNonce(userAgent, mockIp, mockTimestamp.getTime().toString());
    const mockShopData = {
      accessToken: '0a5452dsaf0as9ba65acs43e99b1711b',
      domain: 'chatbotproj.myshopify.com',
    };

    const nextShop = new ShopData();
    nextShop.accessToken = mockShopData.accessToken;
    nextShop.domain = mockShopData.domain;

    await shopifyStoreService.storeShop(nextShop);

    const storedShop = await shopifyStoreService.getShopByDomain(mockShopData.domain);

    expect(storedShop).to.exist;
    expect(storedShop.domain).to.equal(mockShopData.domain);
    expect(storedShop.accessToken).to.equal(mockShopData.accessToken);

    await controller.uninstallCallback({ domain: mockShopData.domain, accessToken: mockShopData.accessToken });

    const uninstalledShop = await shopifyStoreService.getShopByDomain(mockShopData.domain);

    expect(uninstalledShop).not.to.exist;

  });
});
