import { Component } from '@nestjs/common';
import { IShopifyUser } from './ShopifyUser';
import { ShopifyAuthCallbacks } from './shopify-auth-callbacks.service';

@Component()
export class ShopifyAuthVoidCallbacks extends ShopifyAuthCallbacks {
    beforeUninstall(shop: string) {
    }
    afterUninstall(shop: string) {
    }
    beforeAuth(shop: string) {
        return Promise.resolve(true);
    }
    afterAuth(shopifyUser: IShopifyUser) {
    }
    beforeInstall(shop: string) {
        return Promise.resolve(true);
    }
    afterInstall(shopifyUser: IShopifyUser) {
    }
}
