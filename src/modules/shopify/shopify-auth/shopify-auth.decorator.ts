import { ReflectMetadata } from '@nestjs/common';

export const ShopifyProtected = () => ReflectMetadata('ShopifyProtected', true);