export interface IShopifyUser {
    accessToken: string;
    domain: string;
}