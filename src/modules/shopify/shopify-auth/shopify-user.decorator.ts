import { createRouteParamDecorator } from '@nestjs/common';

export const ShopifyUser = createRouteParamDecorator((data, req) => {
    return req.shopifyUser;
});