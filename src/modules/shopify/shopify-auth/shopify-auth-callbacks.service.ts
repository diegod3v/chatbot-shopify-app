import { Component } from '@nestjs/common';
import { ShopifyCallbacks } from './ShopifyCallbacks';
import { IShopifyUser } from './ShopifyUser';

@Component()
export abstract class ShopifyAuthCallbacks implements ShopifyCallbacks {
    abstract beforeAuth(shop: string);
    abstract afterAuth(shopifyUser);
    abstract beforeInstall(shop: string);
    abstract afterInstall(shopifyUser: IShopifyUser);
    abstract beforeUninstall(shop: string);
    abstract afterUninstall(shop: string);
}
