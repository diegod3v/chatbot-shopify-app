import { CanActivate, ExecutionContext, Guard } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs/Observable';

@Guard()
export class ShopifyUserGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) { }

    canActivate(req, context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const { parent, handler } = context;
        const isProtected = this.reflector.get<string[]>('ShopifyProtected', handler);
        if (!isProtected) {
            return true;
        }

        const user = req.shopifyUser;
        const isUser = !!(user && user.domain && user.accessToken);
        return isUser;
    }
}