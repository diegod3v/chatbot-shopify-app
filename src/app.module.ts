import { Logger, MiddlewaresConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShopifyAuthController, ShopifyAuthMiddleware, ShopifyDBStoreService, ShopifyModule } from 'modules/shopify';
import { AppController } from './app.controller';
import {
  DB_HOST, DB_LOGS, DB_NAME, DB_PASS, DB_PORT, DB_SYNC,
  DB_USER,
} from './config';
import { ShopifyAppModule } from './modules/shopify-app';
import { ChatbotModule, XmppAuthController, ChatbotXmppComponentService } from './modules/chatbot';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

const DB_CONFIG: PostgresConnectionOptions = {
  type: 'postgres',
  host: DB_HOST,
  port: DB_PORT,
  username: DB_USER,
  password: DB_PASS,
  database: DB_NAME,
  synchronize: DB_SYNC,
  logging: DB_LOGS,
  entities: [
    __dirname + '/**/*.entity{.ts,.js}',
  ],
};

@Module({
  imports: [
    TypeOrmModule.forRoot(DB_CONFIG),
    ShopifyAppModule,
    ChatbotModule,
  ],
  controllers: [AppController, XmppAuthController],
  components: [Logger],
})
export class AppModule {
  constructor(chatbotComponent: ChatbotXmppComponentService) {
    chatbotComponent.start();
  }
}
