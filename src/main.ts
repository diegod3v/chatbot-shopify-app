// tslint:disable-next-line:no-var-requires
require('dotenv').config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // tslint:disable-next-line:no-console
  console.log('APP DOMAIN:', process.env.APP_DOMAIN);
  // tslint:disable-next-line:no-console
  console.log('NODE ENV:', process.env.NODE_ENV);
  // tslint:disable-next-line:no-console
  console.log('APP ENV: ', app.get('env'));

  function rawBodySaver(req, res, buf, encoding) {
    if (buf && buf.length) {
      req.rawBody = buf.toString(encoding || 'utf8');
    }
  }

  app.use(cors());

  app.use(bodyParser.json({ verify: rawBodySaver }));
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());

  app.use(express.static(path.join(__dirname, 'static')));

  const PORT = process.env.PORT || 3000;

  await app.listen(PORT);

}
bootstrap();
