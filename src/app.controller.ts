import { Get, Controller, Query, Res } from '@nestjs/common';
import { ShopifyUser, IShopifyUser } from './modules/shopify';

@Controller()
export class AppController {

  @Get('install')
  home() {
    return 'Hello World';
  }

  @Get()
  async mainApp(@ShopifyUser() user: IShopifyUser, @Query('shop') shop, @Res() res) {
    // tslint:disable-next-line:no-console
    if (user) {
      res.send('hello');
    } else if (shop) {
      res.redirect('/shopify/auth?shop=' + shop);
    } else {
      res.redirect('/install');
    }
  }

}
