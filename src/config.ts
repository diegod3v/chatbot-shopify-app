export const APP_DOMAIN = process.env.APP_DOMAIN;
export const SHOPIFY_APP_BASE_URL = process.env.SHOPIFY_APP_BASE_URL;
export const SHOPIFY_REDIRECT_URL = `${SHOPIFY_APP_BASE_URL}/shopify/auth/callback`;
export const SHOPIFY_UNINSTALL_ROUTE = 'uninstall';
export const SHOPIFY_UNINSTALL_URL = `${SHOPIFY_APP_BASE_URL}/shopify/${SHOPIFY_UNINSTALL_ROUTE}`;
export const SHOPIFY_STOREFRONT_CHAT_SCRIPT_ROUTE = 'chatbotproj.js';
export const SHOPIFY_STOREFRONT_CHAT_SCRIPT_URL = `https://${APP_DOMAIN}/${SHOPIFY_STOREFRONT_CHAT_SCRIPT_ROUTE}`;
export const REQUIRED_PERMISSIONS = 'read_themes,write_themes,read_fulfillments,read_script_tags, write_script_tags,read_orders';
export const SHOPIFY_SECRET = process.env.SHOPIFY_SECRET;
export const SHOPIFY_API_KEY = process.env.SHOPIFY_API_KEY;
export const SESSION_SECRET = process.env.SESSION_SECRET || 'D4s904jas3AJ';
export const B_COST = 10;

// Database Config
export const DB_HOST = process.env.DB_HOST || 'localhost';
export const DB_PORT = (process.env.DB_PORT ? parseInt(process.env.DB_PORT, 10) : undefined) || 5432;
export const DB_USER = process.env.DB_USER || 'test';
export const DB_PASS = process.env.DB_PASS || '';
export const DB_NAME = process.env.DB_NAME || 'chatbotproj';
export const DB_SYNC = (typeof process.env.DB_SYNC !== 'undefined') ? (process.env.DB_SYNC === 'true') : false;
export const DB_LOGS = (typeof process.env.DB_LOGS !== 'undefined') ? (process.env.DB_LOGS === 'true') : false;

// XMPP Component Config
export const XMPP_COMPONENT_HOST = process.env.XMPP_COMPONENT_HOST || '127.0.0.1';
export const XMPP_COMPONENT_PORT = process.env.XMPP_COMPONENT_PORT || '5347';
export const XMPP_COMPONENT_DOMAIN = process.env.XMPP_COMPONENT_DOMAIN || 'component.localhost';
export const XMPP_COMPONENT_SECRET = process.env.XMPP_COMPONENT_SECRET || 'mysecretcomponentpassword';
export const XMPP_DOMAIN = 'localhost';

// NLP WIT.AI
export const WIT_KEY = process.env.WIT_KEY || '';

// Mail Server (SendGrid)
export const SG_KEY = process.env.SG_KEY || '';

// Auth Config
export const JWT_SECRET = process.env.JWT_SECRET || 'secret';
